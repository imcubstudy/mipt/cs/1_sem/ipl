#ifndef x86_64be_h
#define x86_64be_h

#include <stdio.h>
#include "cipl.h"

char*    x86_64_compile_asm( prog_list_t* prog_list );
char*    x86_64_compile_function( function_t* function );
char*    x86_64_compile_instruction( instruction_t*    instruction );

bool     x86_64_get_executable( char* const outfile_name,
                               unsigned char* const binary, size_t nbytes );

unsigned char* x86_64_get_binary( char* const asm_code, size_t* nbytes );

#endif /* x86_64be_h */
