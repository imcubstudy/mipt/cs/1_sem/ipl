#include "x86_64be.h"
#include <cstdio>
#include "fe.h"

#define ___IN_PTR       0x10000136e
#define ___OUT_PTR      0x10000138e
#define ___LESS_PTR     0x1000013b7
#define ___GREATER_PTR  0x1000013dc
#define ___EQUAL_PTR    0x100001401
#define ___NOP_PTR      0x100001434
#define ___OPC_PTR_SZ   4

bool     x86_64_get_executable( char* const outfile_name,
                               unsigned char* const binary, size_t nbytes )
{
    assert( outfile_name );
    assert( binary );
    assert( nbytes > 8 );
    
    FILE*   dummy   = fopen( "dummy", "r" );
    long    size    = file_size( dummy );
    
    unsigned char*  buffer  = (unsigned char*)calloc( sizeof(char), size + 10 );
    
    assert( buffer );
    
    fread( buffer, sizeof(char), size, dummy );
    fclose( dummy );
    
    unsigned char*   startpos = nullptr;
    for( long i = 0; i < size; ++i )
        if( *(uint64_t*)(buffer + i) == (uint64_t)0xDEDEDADEDEDAEDAD )
        {
            startpos = buffer + i;
            break;
        }
    
    assert( startpos != nullptr );
    
    for( size_t i = 0; i < nbytes; ++i )
    {
        *(startpos + i) = *(binary + i);
    }
    
    FILE*   outfile  = fopen( outfile_name, "w" );
    fwrite( buffer, sizeof(char), size, outfile );
    fclose( outfile );
    
    char    syscmd[MAX_MSG_LEN$] = {};
    sprintf( syscmd, "chmod a+x %s", outfile_name );
    
    system( syscmd );
    
    return true;
}

struct  label_t
{
    char    name[1000]  = {};
    int64_t absptr      = 0;
    int64_t request     = 0;
};

int     print_label( FILE* ofstream, const void* function )
{ return 1; }

int     cmp_label( const void* a, const void* b )
{
    label_t     la  = *(label_t*)a;
    label_t     lb  = *(label_t*)b;
    
    return (int)strcmp( la.name, lb.name );
}

int     copy_label( void*    dest, const void* src )
{
    memcpy( dest, src, sizeof(label_t) );
    return 1;
}

int     destruct_label( void*    function )
{ return 0; }

const data_funcs_t label_funcs = {
    print_label,
    cmp_label,
    copy_label,
    destruct_label
};


unsigned char x86_64_opcodes[40] = {};
#define scanfor( str, ... )                                                     \
({                                                                              \
    int __bunny = 0;                                                            \
    sscanf( scanner, str "%*[ \n\t]%n", ##__VA_ARGS__, &__bunny );              \
    scanner += __bunny;                                                         \
    __bunny != 0;                                                               \
})

#define setbyte( byte ) ({ *(binary + *nbytes) = (unsigned char)(byte); *nbytes += 1; })

#define __setmbytes( n )                                                        \
({                                                                              \
    for( int i = 0; i < n; ++i )                                                \
        setbyte( x86_64_opcodes[i] );                                           \
})

#define __bytearr( ... )                                                        \
({                                                                              \
    unsigned char __t[] = { __VA_ARGS__ };                                      \
    int     sz = sizeof( __t );                                                 \
    memcpy( x86_64_opcodes, __t, sz );                                          \
    sz;                                                                         \
})

#define setbytes( ... ) __setmbytes( __bytearr(__VA_ARGS__) )
unsigned char* x86_64_get_binary( char* const asm_code, size_t* nbytes )
{
    assert( asm_code && nbytes );
    
    *nbytes = 0;
    
    label_t     poisoned_lable = { "_$POISON$_", 0, 0 };
    
    MAKE_LIST( label_values,   label_t, label_funcs, poisoned_lable, LST_DEFAULT_CAPACITY );
    MAKE_LIST( label_requests, label_t, label_funcs, poisoned_lable, LST_DEFAULT_CAPACITY );
    
    label_t known_labels[] =
    {
        { "___IN",      ___IN_PTR,      0 },
        { "___OUT",     ___OUT_PTR,     0 },
        { "___LESS",    ___LESS_PTR,    0 },
        { "___GREATER", ___GREATER_PTR, 0 },
        { "___EQUAL",   ___EQUAL_PTR,   0 }
    };
    
    uint64_t nop_start_addr = ___NOP_PTR;
    
    for( int i = 0; i < sizeof( known_labels ) / sizeof( label_t ); ++i )
    {
        int dummy;
        lst_push_t( label_values, &known_labels[i], &dummy );
    }
    
    unsigned char binary[MAX_ASM_LEN] = {};
    
    char    str_dummy[MAX_ASM_LEN]  = {};
    int     int_dummy               = 0;
    double  dbl_dummy               = 0;
    
    
    size_t  len = strlen( asm_code );
    char*   scanner = asm_code;
    bool    goon    = true;
    while( scanner < asm_code + len - 3 && goon )
    {
        if     ( scanfor( "push   rbp"      ) ) setbyte ( 0x55 );
        else if( scanfor( "mov    rbp, rsp" ) ) setbytes( 0x48, 0x89, 0xe5 );
        else if( scanfor( "sub    rsp, 8d"  ) ) setbytes( 0x48, 0x83, 0xec, 0x08 );
        else if( scanfor( "mov    rax, __float64__(%lg)", &dbl_dummy ) )
        {
            setbytes( 0x48, 0xb8 );
            for( int i = 0; i < sizeof(dbl_dummy); ++i )
            {
                setbyte( (*(uint64_t*)(&dbl_dummy) >> i * 8) & 0xFF );
            }
        }
        else if( scanfor( "mov    qword [rsp], rax" ) ) setbytes( 0x48, 0x89, 0x04, 0x24 );
        else if( scanfor( "lea    rsi, [rbp %dd]", &int_dummy ) )
        {
            setbytes( 0x48, 0x8d, 0x75 );
            setbyte(  (char)int_dummy );
        }
        else if( scanfor( "add    rsp, %dd", &int_dummy ) )
        {
            setbytes( 0x48, 0x81, 0xc4 );
            for( int i = 0; i < sizeof(int_dummy); ++i )
            {
                setbyte( (*(uint32_t*)(&int_dummy) >> i * 8) & 0xFF );
            }
        }
        else if( scanfor( "mov    rsp, rbp" ) ) setbytes( 0x48, 0x89, 0xec );
        else if( scanfor( "pop    rbp" ) )      setbyte ( 0x5d );
        else if( scanfor( "ret" ) )             setbyte ( 0xc3 );
        else if( scanfor( "mov    rax, qword [rbp %dd]", &int_dummy ) )
        {
            setbytes( 0x48, 0x8b, 0x45 );
            setbyte( (char)int_dummy );
        }
        else if( scanfor( "mov    qword [rbp %dd], rax", &int_dummy ) )
        {
            setbytes( 0x48, 0x89, 0x45 );
            setbyte( (char)int_dummy );
        }
        else if( scanfor( "push   rax" ) ) setbyte( 0x50 );
        else if( scanfor( "mov    rax, qword [rsp + 8d]" ) )
            setbytes( 0x48, 0x8b, 0x44, 0x24, 0x08 );
        else if( scanfor( "mov    rax, qword [rsp]" ) ) setbytes( 0x48, 0x8b, 0x04, 0x24 );
        else if( scanfor( "fld    qword [rsp]" ) ) setbytes( 0xdd, 0x04, 0x24 );
        else if( scanfor( "fstp   qword [rsp]" ) ) setbytes( 0xdd, 0x1c, 0x24 );
        else if( scanfor( "fldz" ) )               setbytes( 0xd9, 0xee );
        else if( scanfor( "fcompp" ) )             setbytes( 0xde, 0xd9 );
        else if( scanfor( "fstsw  ax" ) )          setbytes( 0x9b, 0xdf, 0xe0 );
        else if( scanfor( "sahf" ) )               setbyte ( 0x9e );
        else if( scanfor( "fmul   qword [rsp]" ) ) setbytes( 0xdc, 0x0c, 0x24 );
        else if( scanfor( "fdiv   qword [rsp]" ) ) setbytes( 0xdc, 0x34, 0x24 );
        else if( scanfor( "fadd   qword [rsp]" ) ) setbytes( 0xdc, 0x04, 0x24 );
        else if( scanfor( "fsub   qword [rsp]" ) ) setbytes( 0xdc, 0x24, 0x24 );
        else if( scanfor( "fsqrt" ) )              setbytes( 0xd9, 0xfa );
        else if( scanfor( "fsin" ) )               setbytes( 0xd9, 0xfa );
        else if( scanfor( "fcos" ) )               setbytes( 0xd9, 0xfa );
        else if( scanfor( "fld    qword [rsp + 8d]" ) )
            setbytes( 0xdd, 0x44, 0x24, 0x08 );
        else if( scanfor( "fstp   qword [rsp + 8d]" ) )
            setbytes( 0xdd, 0x5c, 0x24, 0x08 );
        else if( scanfor( "call   %s", str_dummy ) )
        {
            setbyte( 0xe8 );
            
            label_t new_label = {};
            new_label.request = *nbytes;
            strcpy( new_label.name, str_dummy );
            lst_push_t( label_requests, &new_label, &int_dummy );
            
            *nbytes += 4;
        }
        else if( scanfor( "jz     %s", str_dummy ) )
        {
            setbytes( 0x0f, 0x84 );
            
            label_t new_label = {};
            new_label.request = *nbytes;
            strcpy( new_label.name, str_dummy );
            lst_push_t( label_requests, &new_label, &int_dummy );
            
            *nbytes += 4;
        }
        else if( scanfor( "jmp    %s", str_dummy ) )
        {
            setbyte( 0xe9 );
            
            label_t new_label = {};
            new_label.request = *nbytes;
            strcpy( new_label.name, str_dummy );
            lst_push_t( label_requests, &new_label, &int_dummy );
            
            *nbytes += 4;
        }
        else if( scanfor( "%[^:]:", str_dummy ) )
        {
            label_t new_label = {};
            new_label.absptr = *nbytes + nop_start_addr;
            strcpy( new_label.name, str_dummy );
            if( find_by_value( label_values, &new_label ) == -1 )
                lst_push_t( label_values, &new_label, &int_dummy );
        }
        else goon = false;
    }
    
    size_t  real_nbytes = *nbytes;
    
    size_t  requests = lst_size( label_requests );
    
    for( size_t i = 0; i < requests; ++i )
    {
        label_t     tlbl = {};
        lst_get_elem( label_requests, &tlbl, (int)i );
        
        *nbytes = tlbl.request;
        
        int pos = find_by_value( label_values, &tlbl );
        assert( pos != -1 );
        
        lst_get_elem( label_values, &tlbl, pos );
        
        int64_t shift = tlbl.absptr - ( *nbytes + nop_start_addr ) - ___OPC_PTR_SZ;
        
        for( int i = 0; i < 4; ++i )
            setbyte( (*(uint32_t*)(&shift) >> i * 8) & 0xFF );
    }
    
    destruct_list( label_values );
    destruct_list( label_requests );
    *nbytes = real_nbytes;
    unsigned char* res = (unsigned char*)calloc( sizeof(char), *nbytes );
    assert( res != nullptr );
    memcpy( res, binary, *nbytes );
    
    return res;
}
#undef __setbyte
#undef scanfor
#undef __setmbytes
#undef setbytes
#undef __bytearr
////////////////////////////////////////////////////////////////////////////
/////////////////////////  FILE GLOBALS    ////////////////////////////////////

list_t*        x86_64_cur_flist        = nullptr;

////////////////////////////////////////////////////////////////////////////

char*    x86_64_compile_asm( prog_list_t* prog_list )
{
    assert( prog_list );
    
    char        res_asm_a[MAX_ASM_LEN]     = {};
    
    list_t*     function_list            = prog_list->function_list;
    int         function_count           = lst_size( function_list ) - 1;
    
    int           main_f_idx             = 0;
    function_t    main_f                 = { "main", 0, nullptr, nullptr };
    assert( "Undefined reference to main" &&
           ({ main_f_idx = find_by_value( function_list, &main_f ); }) != -1 );
    
    ///////////////////////////////////
    x86_64_cur_flist        =     function_list;
    ///////////////////////////////////
    
    lst_get_elem( function_list, &main_f, main_f_idx );
    
    char*        func_asm        = x86_64_compile_function( &main_f );
    
    strcat( res_asm_a, func_asm );
    
    free( func_asm );
    function_funcs.destructor( &main_f );
    
    lst_erase( function_list, main_f_idx );
    
    lst_normalize( function_list );
    
    for( int i = 0; i < function_count; ++i )
    {
        function_t        function    = { {}, -1, nullptr, nullptr };
        
        lst_get_elem( function_list, &function, i );
        
        char*        func_asm        = x86_64_compile_function( &function );
        
        strcat( res_asm_a, func_asm );
        
        free( func_asm );
        function_funcs.destructor( &function );
    }
    
    /////////////////////////////////
    x86_64_cur_flist        =     nullptr;
    /////////////////////////////////
    
    int         len         = strlen( res_asm_a );
    char*       res_asm     = (char*)calloc( sizeof( char ), len * 2 );
    
    strcpy( res_asm, res_asm_a );
    
    return         res_asm;
}

////////////////////////////////////////////////////////////////////////////
/////////////////////////  FILE GLOBALS    /////////////////////////////////

function_t*     x86_64_cur_function         = nullptr;
list_t*         x86_64_cur_vars             = nullptr;
int             x86_64_cur_instruction      = -1;
int             x86_64_cur_max_shift        = -1;

////////////////////////////////////////////////////////////////////////////

char*    x86_64_compile_function( function_t* function )
{
    assert( function );
    
    ////////////////////////////////////////////////////////////////////////////////
    variable_t    poisoned_var = {};//    = { "$$POISON$$", INT_MIN };
    strcpy( poisoned_var.name, "$$POISON$$" ); poisoned_var.shift = INT_MIN;
    MAKE_LIST( _tlist, variable_t, var_funcs, poisoned_var, LST_DEFAULT_CAPACITY );
    
    x86_64_cur_vars        = _tlist;
    x86_64_cur_function    = function;
    x86_64_cur_instruction = 0;
    x86_64_cur_max_shift   = 0;
    ////////////////////////////////////////////////////////////////////////////////
    
    char        res_asm_a[MAX_ASM_LEN]    = {};
    
    sprintf( res_asm_a, "_%s_f%d:\n"
                        "push   rbp\n"
                        "mov    rbp, rsp\n"
                        "sub    rsp, 8d\n",
            function->name, function->params_count );
    
    list_t*        instruction_list    = function->instruction_list;
    int            instruction_count   = lst_size( instruction_list );
    
    for( int i = 0; i < function->params_count; ++i )
    {
        lex_t        tlex    = {};
        lst_get_elem( function->params_list, &tlex, i );
        
        variable_t        var_t    = {};
        strcpy( var_t.name, tlex.value );
        var_t.shift                =    +8 * ( i + 2 ); // + 2 for ret addr and prev rbp
        
        int dummy = 0;
        
        lst_push_t( x86_64_cur_vars, &var_t, &dummy );
    }
    
    while( x86_64_cur_instruction < instruction_count )
    {
        instruction_t        instr    = { _POISON, nullptr };
        
        lst_get_elem( instruction_list, &instr, x86_64_cur_instruction );
        
        char*        instr_asm         = x86_64_compile_instruction( &instr );
        strcat( res_asm_a, instr_asm );
        
        free( instr_asm );
        instruction_funcs.destructor( &instr );
    }
    
    char    t_res[MAX_ASM_LEN] = {};
    sprintf( t_res, "mov    rsp, rbp\n"
                    "pop    rbp\n"
                    "ret\n" );
    
    strcat( res_asm_a,  t_res );
    
    int         len            = strlen( res_asm_a );
    char*       res_asm        = (char*)calloc( sizeof( char ), len + 1 );
    
    strcpy( res_asm, res_asm_a );
    
    ///////////////////////////////////////////////////////////////////////////////
    destruct_list( x86_64_cur_vars );
    
    x86_64_cur_vars        = nullptr;
    x86_64_cur_function    = nullptr;
    x86_64_cur_instruction = -1;
    x86_64_cur_max_shift   = -1;
    ///////////////////////////////////////////////////////////////////////////////
    
    printf( "Function %s_%d compiled\n", function->name, function->params_count );
    return         res_asm;
}

char*   x86_64_compile_EXP( node_t* root );
char*   x86_64_get_var_rptr( char* const var_name, char* const reg_name );
char*   x86_64_compile_CALL( node_t* root );

char*   x86_64_compile_instruction( instruction_t*    instruction )
{
    assert( instruction );
    
    char        res_asm_a[MAX_ASM_LEN]     = {};
    
    node_t*     root    = instruction->instruction_tree->root;
    
    switch( instruction->type )
    {
        case ASSN:
        {
            char*    exp_res        = x86_64_compile_EXP( root->right );
            
            variable_t tvar = {};
            
            strcpy( tvar.name, ((lex_t*)(root->left->data))->value );
            
            int pos = find_by_value( x86_64_cur_vars, &tvar );
            if( pos == -1 )
            {
                printf( "Undefined reference to %s in func %s\n", tvar.name,
                       x86_64_cur_function->name );
                assert( !"Compilation error" );
            }
            
            lst_get_elem( x86_64_cur_vars, &tvar, pos );
            sprintf( res_asm_a,
                        "%s"    // exp result to [rsp],
                        "mov    rax, qword [rsp]\n"
                        "mov    qword [rbp %+dd], rax\n",
                    exp_res, tvar.shift );
            
            free( exp_res );
            
            x86_64_cur_instruction++;
            
            break;
        }
        case KW:
        {
            keyword_t    keyword    = *(keyword_t*)(((lex_t*)root->data)->value);
            
            switch( keyword )
            {
                case VAR:
                {
                    char    var_name[MAX_ASM_LEN]    = {};
                    strcpy( var_name, ((lex_t*)(root->left->data))->value );
                    
                    variable_t    var_t     = {};
                    strcpy( var_t.name, var_name );
                    
                    int     pos             = find_by_value( x86_64_cur_vars, &var_t );
                    if( pos != -1 )
                    {
                        printf( "Double variable declaration\n" );
                        assert( !"Compilation error" );
                    }
                    
                    var_t.shift             =  -8 * ( 1 + x86_64_cur_max_shift++ );
                    
                    int     _pos            = 0;
                    lst_push_t( x86_64_cur_vars, &var_t, &_pos );
                    
                    char*    exp_res  = x86_64_compile_EXP( root->right );
                    
                    sprintf( res_asm_a,
                            "%s"
                            "sub    rsp, 8d\n",
                            exp_res );
                    
                    free( exp_res );
                    
                    x86_64_cur_instruction++;
                    
                    break;
                }
                case IN:
                {
                    variable_t    tvar        = {};
                    
                    strcpy( tvar.name, ((lex_t*)root->left->data)->value );
                    
                    int pos = find_by_value( x86_64_cur_vars, &tvar );
                    if( pos == -1 )
                    {
                        printf( "Undefined reference to %s in func %s\n", tvar.name,
                               x86_64_cur_function->name );
                        assert( !"Compilation error" );
                    }
                    
                    lst_get_elem( x86_64_cur_vars, &tvar, pos );
                    
                    sprintf( res_asm_a,
                            "lea    rsi, [rbp %+dd]\n"
                            "call   ___IN\n",
                            tvar.shift );

                    x86_64_cur_instruction++;
                    
                    break;
                }
                case OUT:
                {
                    variable_t    tvar        = {};
                    
                    strcpy( tvar.name, ((lex_t*)root->left->data)->value );
                    
                    int pos = find_by_value( x86_64_cur_vars, &tvar );
                    if( pos == -1 )
                    {
                        printf( "Undefined reference to %s in func %s\n", tvar.name,
                               x86_64_cur_function->name );
                        assert( !"Compilation error" );
                    }
                    
                    lst_get_elem( x86_64_cur_vars, &tvar, pos );
                    
                    sprintf( res_asm_a,
                            "lea    rsi, [rbp %+dd]\n"
                            "call   ___OUT\n",
                            tvar.shift );
                    
                    x86_64_cur_instruction++;
                    
                    break;
                }
                case RETURN:
                {
                    char    var_name[MAX_ASM_LEN]    = {};
                    strcpy( var_name, ((lex_t*)root->left->data)->value );
                    
                    char*        var        = x86_64_get_var_rptr( var_name, "rax" );
                    
                    sprintf( res_asm_a,
                            "%s"
                            "mov    rsp, rbp\n"
                            "pop    rbp\n"
                            "ret\n",
                            var );
                    
                    free( var );
                    
                    x86_64_cur_instruction++;
                    
                    break;
                }
                case IF:
                {
                    int     instr_delt = (int)(*(double*)(((lex_t*)root->right->data)->value));
                    
                    int     if_id    = *(int*)( ((lex_t*)root->data)->value + sizeof( keyword_t ) );
                    char    if_label[MAX_ASM_LEN]    = {};
                    
                    sprintf( if_label, "_endif_%d", if_id );
                    
                    char*    exp        = x86_64_compile_EXP( root->left );
                    
                    sprintf( res_asm_a,
                            "%s"      // exp to [rsp]
                            "fld    qword [rsp]\n"
                            "fldz\n"
                            "fcompp\n"
                            "fstsw  ax\n"
                            "sahf\n"
                            "jz     %s\n",
                            exp, if_label );
                    
                    free( exp );
                    
                    x86_64_cur_instruction++;
                    
                    int     prev_var_ls      = lst_size( x86_64_cur_vars );
                    
                    int     start_pos        = x86_64_cur_instruction;
                    
                    while( x86_64_cur_instruction - start_pos < instr_delt )
                    {
                        instruction_t        instr    = { _POISON, nullptr };
                        
                        lst_get_elem( x86_64_cur_function->instruction_list, &instr,
                                      x86_64_cur_instruction );
                        
                        char*        instr_asm         = x86_64_compile_instruction( &instr );
                        strcat( res_asm_a, instr_asm );
                        
                        free( instr_asm );
                        instruction_funcs.destructor( &instr );
                    }
                    
                    int        new_var_ls = lst_size( x86_64_cur_vars );
                    
                    for( int i = 0; i < new_var_ls - prev_var_ls; ++i )
                    {
                        variable_t        trash;
                        int               trashpos;
                        
                        lst_pop_t( x86_64_cur_vars, &trash, &trashpos );
                        
                        x86_64_cur_max_shift--;
                    }
                    
                    int     stk_shift = 8 * ( new_var_ls - prev_var_ls );
                    
                    char    tmp_res[MAX_ASM_LEN]    = {};
                    
                    sprintf( tmp_res,
                            "add    rsp, %dd\n"
                            "%s:\n",
                            stk_shift, if_label );
                    
                    strcat( res_asm_a, tmp_res );
                    
                    break;
                }
                case WHILE:
                {
                    int  instr_delt  = (int)(*(double*)(((lex_t*)root->right->data)->value));
                    
                    int  while_id    = *(int*)( ((lex_t*)root->data)->value + sizeof( keyword_t ) );
                    char while_label[MAX_ASM_LEN]    = {};
                    
                    sprintf( while_label, "_wcycle_%d", while_id );
                    
                    char*    exp        = x86_64_compile_EXP( root->left );
                    
                    sprintf( res_asm_a,
                            "%s:\n"
                            "%s"      // exp to [rsp]
                            "fld    qword [rsp]\n"
                            "fldz\n"
                            "fcompp\n"
                            "fstsw  ax\n"
                            "sahf\n"
                            "jz     %s_exit\n", while_label, exp, while_label );
                    
                    free( exp );
                    
                    x86_64_cur_instruction++;
                    
                    int     prev_var_ls    = lst_size( x86_64_cur_vars );
                    
                    int     start_pos      = x86_64_cur_instruction;
                    
                    while( x86_64_cur_instruction - start_pos < instr_delt )
                    {
                        instruction_t        instr    = { _POISON, nullptr };
                        
                        lst_get_elem( x86_64_cur_function->instruction_list, &instr,
                                     x86_64_cur_instruction );
                        
                        char*        instr_asm        = x86_64_compile_instruction( &instr );
                        strcat( res_asm_a, instr_asm );
                        
                        free( instr_asm );
                        instruction_funcs.destructor( &instr );
                    }
                    
                    int        new_var_ls = lst_size( x86_64_cur_vars );
                    
                    for( int i = 0; i < new_var_ls - prev_var_ls; ++i )
                    {
                        variable_t        trash;
                        int               trashpos;
                        
                        lst_pop_t( x86_64_cur_vars, &trash, &trashpos );
                        
                        x86_64_cur_max_shift--;
                    }
                    
                    int     stk_shift = 8 * ( new_var_ls - prev_var_ls );
                    
                    char    tmp_res[MAX_ASM_LEN]    = {};
                    
                    sprintf( tmp_res,
                            "add    rsp, %dd\n"
                            "jmp    %s\n"
                            "%s_exit:\n",
                            stk_shift, while_label, while_label );
                    
                    strcat( res_asm_a, tmp_res );
                    
                    break;
                    
                }
                default:
                {
                    printf( "Unexpected keyword\n" );
                    assert( !"Compilation error" );
                    break;
                }
            }
            
            break;
        }
        case CALL:
        {
            char*        call    =     x86_64_compile_CALL( root );
            sprintf( res_asm_a, "%s", call );
            free( call );
            
            x86_64_cur_instruction++;
            
            break;
        }
        default:
        {
            printf( "What?\n" );
            assert( !"Compilation error" );
            break;
        }
    }
    
    int          len            = strlen( res_asm_a );
    char*        res_asm        = (char*)calloc( sizeof( char ), len + 1 );
    
    strcpy( res_asm, res_asm_a );
    
    return         res_asm;
}

char*   x86_64_compile_EXP( node_t* root )
{
    assert( root );
    
    char         res_asm_a[MAX_ASM_LEN]    = {};
    
    lex_t        rootlex        = {};
    lex_funcs.copy_func( &rootlex, root->data );
    
    switch( rootlex.type )
    {
        case VARIABLE:
        {
            int        rsize    = count_node_childs( root );
            if( rsize == 1 )
            {
                char*    var        = x86_64_get_var_rptr( rootlex.value, "rax" );
                sprintf( res_asm_a,
                        "%s"
                        "mov    qword [rsp], rax\n", var );
                
                free( var );
            }
            else
            {
                char*        call    = x86_64_compile_CALL( root );
                sprintf( res_asm_a,
                        "%s"
                        "mov    qword [rsp], rax\n", call );
                free( call );
            }
            break;
        }
        case CONSTANT:
        {
            sprintf( res_asm_a,
                    "mov    rax, __float64__(%.5lf)\n"
                    "mov    qword [rsp], rax\n",
                    *(double*)rootlex.value );
            break;
        }
        case OPERATOR:
        {
            char*    rpart    = x86_64_compile_EXP( root->right );
            char*    lpart    = x86_64_compile_EXP( root->left );
            
            char    action[MAX_ASM_LEN]   = {};
            char    op                    = rootlex.value[0];
            bool    costyl                = false;
            
            if     ( op == '+' ) strcpy( action, "fadd   qword [rsp]\n" );
            else if( op == '-' ) strcpy( action, "fsub   qword [rsp]\n" );
            else if( op == '*' ) strcpy( action, "fmul   qword [rsp]\n" );
            else if( op == '/' ) strcpy( action, "fdiv   qword [rsp]\n" );
            else if( strchr( ">=<", op ) != nullptr )
            {
                costyl = true;
                if     ( op == '>' ) strcpy( action, "call   ___GREATER\n" );
                else if( op == '<' ) strcpy( action, "call   ___LESS\n" );
                else if( op == '=' ) strcpy( action, "call   ___EQUAL\n" );
            }
            else
            {
                printf( "Unexpected operator in expression" );
                assert( !"Compilation error" );
            }
            
            if( !costyl )
            {
                sprintf( res_asm_a,
                        "%s"    // lpart
                        "sub    rsp, 8d\n"
                        "%s"    // rpart
                        "fld    qword [rsp + 8d]\n"
                        "%s"    // action
                        "fstp   qword [rsp + 8d]\n"
                        "add    rsp, 8d\n", lpart, rpart, action );
            }
            else
            {
                sprintf( res_asm_a,
                        "%s"    // lpart
                        "sub    rsp, 8d\n"
                        "%s"    // rpart
                        "mov    rax, qword [rsp + 8d]\n"
                        "push   rax\n"
                        "mov    rax, qword [rsp + 8d]\n"
                        "push   rax\n"
                        "%s"    // call costyl
                        "add    rsp, 24d\n"
                        "mov    qword [rsp], rax\n", lpart, rpart, action );
            }
            
            free( lpart );
            free( rpart );
            
            break;
        }
        case KEYWORD:
        {
            keyword_t   keyword  = *(keyword_t*)rootlex.value;
            char*       lpart    = x86_64_compile_EXP( root->left );
            sprintf( res_asm_a,
                    "%s"
                    "fld    qword [rsp]\n",
                    lpart );
            free( lpart );
            
            if     ( keyword == SIN  )  strcat( res_asm_a, "fsin\n" );
            else if( keyword == COS  )  strcat( res_asm_a, "fcos\n" );
            else if( keyword == SQRT )  strcat( res_asm_a, "fsqrt\n" );
            else assert( !"Compilation error: whaaaaaaaaat!?" );
        
            strcat( res_asm_a, "fstp   qword [rsp]\n" );
            break;
        }
    }
    
    int     len         = strlen( res_asm_a );
    char*   res_asm     = (char*)calloc( sizeof( char ), len + 1 );
    
    strcpy( res_asm, res_asm_a );
    
    return res_asm;
}

char*   x86_64_get_var_rptr( char* const var_name, char* const reg_name )
{
    assert( var_name );
    variable_t    tvar        = {};
    
    strcpy( tvar.name, var_name );
    
    int pos = find_by_value( x86_64_cur_vars, &tvar );
    if( pos == -1 )
    {
        printf( "Undefined reference to %s in func %s\n", var_name,
               x86_64_cur_function->name );
        assert( !"Compilation error" );
    }
    
    lst_get_elem( x86_64_cur_vars, &tvar, pos );
    
    char        res_asm_a[MAX_ASM_LEN]    = {};
    
    sprintf( res_asm_a,
            "mov    %s, qword [rbp %+dd]\n",
            reg_name, tvar.shift );
    
    int     len         = strlen( res_asm_a );
    char*   res_asm     = (char*)calloc( sizeof( char ), len + 1 );
    
    strcpy( res_asm, res_asm_a );
    
    return         res_asm;
}

char*   x86_64_compile_CALL( node_t* root )
{
    assert( root );
    
    int     params_count     = count_node_childs( root ) - 1;
    
    function_t        function    = {};
    strcpy( function.name, ((lex_t*)root->data)->value );
    function.params_count         = params_count;
    
    int     pos     = find_by_value( x86_64_cur_flist, &function );
    if( pos == -1 )
    {
        printf( "Undefined reference to %s with %d params\n", function.name, params_count );
        assert( !"Compilation error" );
    }
    
    for( int i = 0; i < params_count; ++i )
        root = root->left;
    
    char        res_asm_a[MAX_ASM_LEN] = {};
    
    for( int i = 0; i < params_count; ++i )
    {
        char*   var        = x86_64_get_var_rptr( ((lex_t*)root->data)->value, "rax" );
        char    tres[MAX_ASM_LEN] = {};
        sprintf( tres,
                "%s"
                "push   rax\n", var );
        
        strcat( res_asm_a, tres );
        free( var );
        root = root->parent;
    }
    
    char    tres[MAX_ASM_LEN]     = {};
    sprintf( tres,
            "call   _%s_f%d\n"
            "add    rsp, %dd\n", function.name, params_count, 8 * params_count );
    
    strcat( res_asm_a, tres );
    
    int     len         = strlen( res_asm_a );
    char*   res_asm     = (char*)calloc( sizeof( char ), len + 1 );
    
    strcpy( res_asm, res_asm_a );
    
    return         res_asm;
}

