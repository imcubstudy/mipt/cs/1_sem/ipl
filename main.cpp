#include <stdio.h>
#include "fe.h"
#include "be.h"
#include "x86_64be.h"
#include "posixint.h"

int main( int argc, char* const argv[] )
{
    if( argc == 0 )
    {
        printf( "%s", usage );
        return 0;
    }

    int parser_result = parse_args( argc, argv );
    if( parser_result != 0 )
        return parser_result;

    char*           src         = get_src( opt_src_filename );
    lex_t*          lex         = get_lex( src );
    prog_list_t*    prog_list   = compile_prog_list( lex );
    free( src );
    free( lex );

    if( opt_dump )
    {
        char dump_filename[MAX_FILENAME_LEN] = {};

        if( opt_dump_filename == nullptr )
             sprintf( dump_filename, "%s_proglist", opt_src_filename );
        else sprintf( dump_filename, "%s", opt_dump_filename );

        FILE*   dump_file = fopen( dump_filename, "w" );
        dump_prog_list( dump_file, prog_list );
        fclose( dump_file );

        if( opt_mkpdf )
        {
            char pdf_cmd[MAX_FILENAME_LEN] = {};
            sprintf( pdf_cmd, "/usr/local/bin/dot -Tpdf %s -o %s.pdf  > /dev/null",
                    dump_filename,
                    dump_filename );
            system( pdf_cmd );
        }
    }

    char*   asm_code = nullptr;
    if( opt_arch == x86_64 )
        asm_code = x86_64_compile_asm( prog_list );
    else if( opt_arch == ISP )
        asm_code = compile_asm( prog_list );
    else assert( !"Woof" );
    
    destruct_prog_list( prog_list );
    
    if( opt_arch == x86_64 && !opt_supress_bin )
    {
        size_t  nbytes = 0;
        unsigned char* binary = x86_64_get_binary( asm_code, &nbytes );
        x86_64_get_executable( opt_out_filename, binary, nbytes );
        free( binary );
    }
    else
    {
        FILE*   outfile = fopen( opt_out_filename, "w" );
        fprintf( outfile, "%s\n", asm_code );
        fclose( outfile );
    }
    
    free( asm_code );
    
    return 0;
}
