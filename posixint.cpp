#include "posixint.h"
#include <cstring>

char    usage[] = "ipl\n"
    "-f --file=[input source file]\n"
    "-a --arch=[x86_64 or isp] (default is isp)\n"
    "-s --supressbin - skip asm->bin translation\n"
    "-d --dump=[dump filename(=%src%proglist)] - dump middle representation\n"
    "-p --pdf - product pdf dump\n"
    "-o --output=[output filename] - output filename (default is a.out)\n";

char    optstring[] = "a:sd::o:f:p";

option  program_options[] = {
    { "pdf",        no_argument,       0, 'p' },
    { "file",       required_argument, 0, 'f' },
    { "arch",       required_argument, 0, 'a' },
    { "supressbin", no_argument,       0, 's' },
    { "dump",       optional_argument, 0, 'd' },
    { "output",     required_argument, 0, 'o' },
    {}
};

char*    opt_src_filename    = nullptr;

arch_t   opt_arch            = ISP;

bool     opt_supress_bin     = false;

bool     opt_dump            = false;
char*    opt_dump_filename   = nullptr;
bool     opt_mkpdf           = false;

char*    opt_out_filename    = nullptr;


int parse_args( int argc, char* const argv[] )
{
    while( true )
    {
        int val = getopt_long( argc, argv, optstring, program_options, nullptr );
        if( val == -1 ) break;
        
        switch( val )
        {
            case 'p':
            {
                opt_mkpdf = true;
                break;
            }
            case 'f': // -f --file=[input source file]
            {
                if( optarg == nullptr )
                {
                    fprintf( stderr, "Source filename?\n" );
                    return EXIT_OPT_ERR;
                }
                opt_src_filename = optarg;
                break;
            }
            case 'a': // -a --arch=[arm64 or isp] (default is isp)
            {
                if( optarg == nullptr )
                {
                    fprintf( stderr, "Arch?\n" );
                    return EXIT_OPT_ERR;
                }
                else if( strcmp( "x86_64", optarg ) == 0 )
                    opt_arch = x86_64;
                else if( strcmp( "isp",   optarg ) == 0 )
                    opt_arch = ISP;
                else
                {
                    fprintf( stderr, "Arch?: x86_64 or isp\n" );
                    return EXIT_OPT_ERR;
                }
                break;
            }
            case 's': // -s --supressbin - skip asm->bin translation
            {
                opt_supress_bin = true;
                break;
            }
            case 'd': // -d --dump=[dump filename(=%src%proglist)] - dump middle representation
            {
                opt_dump            = true;
                opt_dump_filename   = optarg;
                break;
            }
            case 'o': // -o --output=[output filename] - output filename (default is a.out)
            {
                if( optarg == nullptr )
                {
                    fprintf( stderr, "Output filename?\n" );
                    return EXIT_OPT_ERR;
                }
                opt_out_filename = optarg;
                break;
            }
            default:
            {
                fprintf( stderr, "Wut?\n" );
                return EXIT_OPT_ERR;
                break;
            }
        }
    }
    
    if( opt_src_filename == nullptr )
    {
        fprintf( stderr, "No source file specified\n" );
        return EXIT_OPT_ERR;
    }

    return 0;
}
