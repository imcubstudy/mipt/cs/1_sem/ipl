#ifndef posixint_h
#define posixint_h

#include <stdio.h>
#include <getopt.h>


#define MAX_FILENAME_LEN    1000
#define EXIT_OPT_ERR        101

extern char    usage[];

extern char    optstring[];

extern option  program_options[];


extern char*    opt_src_filename;

enum arch_t
{
    ISP     = 0,
    x86_64  = 1
};

extern arch_t   opt_arch;

extern bool     opt_supress_bin;

extern bool     opt_dump;
extern char*    opt_dump_filename;
extern bool     opt_mkpdf;

extern char*    opt_out_filename;


int parse_args( int argc, char* const argv[] );

#endif /* posixint_h */
