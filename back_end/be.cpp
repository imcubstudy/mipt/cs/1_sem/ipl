#include "be.h"
#include <cstring>
#include <cstdlib>

////////////////////////////////////////////////////////////////////////////
/////////////////////////  FILE GLOBALS	////////////////////////////////////

list_t*		be_cur_flist		= nullptr;

////////////////////////////////////////////////////////////////////////////

char*	compile_asm( prog_list_t* prog_list )
{
	assert( prog_list );

	char		res_asm_a[MAX_ASM_LEN] 	= {};

	list_t*		function_list			= prog_list->function_list;
	int 		function_count			= lst_size( function_list );
	
	function_t	main_f					= { "main", 0, nullptr, nullptr };
	assert( "Undefined reference to main" && find_by_value( function_list, &main_f ) != -1 );

	///////////////////////////////////
	be_cur_flist		= 	function_list;
	///////////////////////////////////

	for( int i = 0; i < function_count; ++i )
	{
		function_t		function	= { {}, -1, nullptr, nullptr };

		lst_get_elem( function_list, &function, i );

		char*		func_asm		= compile_function( &function );

		strcat( res_asm_a, func_asm );
		
		free( func_asm );
		function_funcs.destructor( &function );
	}

	/////////////////////////////////
	be_cur_flist		= 	nullptr;	
	/////////////////////////////////

	int			len 		= strlen( res_asm_a );
	char*		res_asm		= (char*)calloc( sizeof( char ), len * 2 );

	sprintf( res_asm, 	"PUSH	0\n"
						"POP	r1\n"
						"CALL 	_main_f0\n"
						"END\n\n"
						
						"LABEL 	_k_a\n"
						"JA		_k_a_t\n"
						"PUSH	0\n"
						"JMP	_k_a_e\n"
						"LABEL	_k_a_t\n"
						"PUSH	1\n"
						"LABEL	_k_a_e\n"
						"RET\n"
						
						"LABEL 	_k_b\n"
						"JB		_k_b_t\n"
						"PUSH	0\n"
						"JMP	_k_b_e\n"
						"LABEL	_k_b_t\n"
						"PUSH	1\n"
						"LABEL	_k_b_e\n"
						"RET\n"
						
						"LABEL 	_k_e\n"
						"JE		_k_e_t\n"
						"PUSH	0\n"
						"JMP	_k_e_e\n"
						"LABEL	_k_e_t\n"
						"PUSH	1\n"
						"LABEL	_k_e_e\n"
						"RET\n"

						"%s", res_asm_a );

	return 		res_asm;
}

////////////////////////////////////////////////////////////////////////////
/////////////////////////  FILE GLOBALS	////////////////////////////////////

function_t*		cur_function		= nullptr;
list_t*			cur_vars			= nullptr;
int				cur_max_shift		= 0;
int				cur_instruction		= -1;

////////////////////////////////////////////////////////////////////////////

char*		compile_function( function_t*	function )
{
	assert( function );
	
	////////////////////////////////////////////////////////////////////////////////
	variable_t	poisoned_var = {};//	= { "$$POISON$$", INT_MIN };
	strcpy( poisoned_var.name, "$$POISON$$" ); poisoned_var.shift = INT_MIN;
	MAKE_LIST( _tlist, variable_t, var_funcs, poisoned_var, LST_DEFAULT_CAPACITY );
	
	cur_vars		= _tlist;
	cur_function	= function;
	cur_instruction = 0;
	cur_max_shift	= 0;
	////////////////////////////////////////////////////////////////////////////////

	char		res_asm_a[MAX_ASM_LEN]	= {};
	
	sprintf( res_asm_a, "LABEL _%s_f%d\n\n", function->name, function->params_count );
	
	list_t*		instruction_list	= function->instruction_list;
	int			instruction_count	= lst_size( instruction_list );

	for( int i = 0; i < function->params_count; ++i )
	{
		lex_t		tlex	= {};
		lst_get_elem( function->params_list, &tlex, i );

		variable_t		var_t	= {};
		strcpy( var_t.name, tlex.value );
		var_t.shift				=	cur_max_shift++;

		int 	_pos			= 0;
		lst_push_t( cur_vars, &var_t, &_pos );
				
		char	tres[MAX_ASM_LEN]	= {};
		sprintf( tres, 	"PUSH	r1\n"
						"PUSH	%d\n"
						"ADD\n"
						"POP	r2\n"
						"POP	[r2]\n\n", var_t.shift );
		strcat( res_asm_a, tres );
	}

	while( cur_instruction < instruction_count )
	{
		instruction_t		instr	= { _POISON, nullptr };

		lst_get_elem( instruction_list, &instr, cur_instruction );

		char*		instr_asm 		= compile_instruction( &instr );
		strcat( res_asm_a, instr_asm );

		free( instr_asm );
		instruction_funcs.destructor( &instr );
	}
	
	strcat( res_asm_a, "RET\n" );

	int 		len			= strlen( res_asm_a );
	char*		res_asm		= (char*)calloc( sizeof( char ), len + 1 );

	strcpy( res_asm, res_asm_a );

	///////////////////////////////////////////////////////////////////////////////
	destruct_list( cur_vars );

	cur_vars		= nullptr;
	cur_function	= nullptr;
	cur_instruction	= -1;
	cur_max_shift	= 0;
	///////////////////////////////////////////////////////////////////////////////
		
	printf( "Function %s_%d compiled\n", function->name, function->params_count );
	return 		res_asm;
}

char*	get_var_rptr( const char* name, int n );
char*	compile_EXP( node_t* root );
char*	compile_CALL( node_t* root );

char*	compile_instruction( instruction_t*	instruction )
{
	assert( instruction );

	char		res_asm_a[MAX_ASM_LEN] 	= {};
	
	node_t*		root	= instruction->instruction_tree->root;
	
	switch( instruction->type )
	{
		case ASSN:
		{
			char*	exp_res		= compile_EXP( root->right );
			
			char	var_name[MAX_ASM_LEN] = {};
			strcpy( var_name, ((lex_t*)(root->left->data))->value );
			
			char*	var			= get_var_rptr( var_name, 2 );

			sprintf( res_asm_a, "\n"
								"%s\n"
								"%s\n"
								"POP	[r2]\n\n", exp_res, var ); 
			free( exp_res );
			free( var );
			
			cur_instruction++;

			break;
		}
		case KW:
		{
			keyword_t	keyword	= *(keyword_t*)(((lex_t*)root->data)->value);
			
			switch( keyword )
			{
				case VAR:
				{
					char	var_name[MAX_ASM_LEN]	= {};
					strcpy( var_name, ((lex_t*)(root->left->data))->value );
					
					variable_t		var_t	= {};
					strcpy( var_t.name, var_name );

					int 	pos				= find_by_value( cur_vars, &var_t );
					if( pos != -1 )
					{
						printf( "Double variable declaration\n" );
						assert( !"Compilation error" );
					}

					var_t.shift				=	cur_max_shift++;

					int 	_pos			= 0;
					lst_push_t( cur_vars, &var_t, &_pos );

					char*	var				= get_var_rptr( var_name, 2 );
					char*	exp_res			= compile_EXP( root->right );
					
					sprintf( res_asm_a, "%s\n"
										"%s\n"
										"POP	[r2]\n\n", exp_res, var );
					
					free( var );
					free( exp_res );
					
					cur_instruction++;

					break;
				}
				case IN:
				{
					char	var_name[MAX_ASM_LEN] 	= {};
					strcpy( var_name, ((lex_t*)root->left->data)->value );

					char*	var			= get_var_rptr( var_name, 2 );

					sprintf( res_asm_a, "IN\n"
										"%s\n"
										"POP	[r2]\n\n", var );

					free( var );

					cur_instruction++;

					break;
				}
				case OUT:
				{
					char	var_name[MAX_ASM_LEN] 	= {};
					strcpy( var_name, ((lex_t*)root->left->data)->value );

					char*	var			= get_var_rptr( var_name, 2 );

					sprintf( res_asm_a, "%s\n"
										"PUSH	[r2]\n"
										"OUT\n"
										"POP	[r2]\n\n", var );

					free( var );

					cur_instruction++;

					break;
				}
				case RETURN:
				{
					char	var_name[MAX_ASM_LEN]	= {};
					strcpy( var_name, ((lex_t*)root->left->data)->value );
					
					char*		var		= get_var_rptr( var_name, 2 );

					sprintf( res_asm_a, "%s\n"
										"PUSH	[r2]\n"
										"POP	r3\n"
										"RET\n\n", var );
					
					free( var );

					cur_instruction++;

					break;
				}
				case IF:
				{
					int		instr_delt		= (int)(*(double*)(((lex_t*)root->right->data)->value));
					
					int		if_id	= *(int*)( ((lex_t*)root->data)->value + sizeof( keyword_t ) );
					char	if_label[MAX_ASM_LEN]	= {};

					sprintf( if_label, "_endif_%d", if_id );
					
					char*	exp		= compile_EXP( root->left );
					
					sprintf( res_asm_a, "%s\n"
										"PUSH	0\n"
										"JE		%s\n\n", exp, if_label ); 
					
					free( exp );

					cur_instruction++;
					
					int 	prev_var_ls		= lst_size( cur_vars );

					int 	start_pos		= cur_instruction;

					while( cur_instruction - start_pos < instr_delt )
					{
						instruction_t		instr	= { _POISON, nullptr };

						lst_get_elem( cur_function->instruction_list, &instr, cur_instruction );

						char*		instr_asm 		= compile_instruction( &instr );
						strcat( res_asm_a, instr_asm );

						free( instr_asm );
						instruction_funcs.destructor( &instr );
					}
				
					int		new_var_ls = lst_size( cur_vars ); 

					for( int i = 0; i < new_var_ls - prev_var_ls; ++i )
					{	
						variable_t		trash;
						int				trashpos;
						
						lst_pop_t( cur_vars, &trash, &trashpos );

						cur_max_shift--;
					}	

					char	tmp_res[MAX_ASM_LEN]	= {};
					sprintf( tmp_res, "LABEL	%s\n\n", if_label );

					strcat( res_asm_a, tmp_res );

					break;
				}
				case WHILE:
				{
					int		instr_delt		= (int)(*(double*)(((lex_t*)root->right->data)->value));
					
					int		while_id	= *(int*)( ((lex_t*)root->data)->value + sizeof( keyword_t ) );
					char	while_label[MAX_ASM_LEN]	= {};

					sprintf( while_label, "_wcycle_%d", while_id );
					
					char*	exp		= compile_EXP( root->left );
					
					sprintf( res_asm_a, "LABEL	%s\n"
										"%s\n"
										"PUSH	0\n"
										"JE		%s_exit\n\n", while_label, exp, while_label ); 
					
					free( exp );

					cur_instruction++;
					
					int 	prev_var_ls	= lst_size( cur_vars );
					
					int 	start_pos		= cur_instruction;

					while( cur_instruction - start_pos < instr_delt )
					{
						instruction_t		instr	= { _POISON, nullptr };

						lst_get_elem( cur_function->instruction_list, &instr, cur_instruction );

						char*		instr_asm 		= compile_instruction( &instr );
						strcat( res_asm_a, instr_asm );

						free( instr_asm );
						instruction_funcs.destructor( &instr );
					}	
					
					int		new_var_ls = lst_size( cur_vars ); 

					for( int i = 0; i < new_var_ls - prev_var_ls; ++i )
					{	
						variable_t		trash;
						int				trashpos;

						lst_pop_t( cur_vars, &trash, &trashpos );
						
						cur_max_shift--;
					}	

					char	tmp_res[MAX_ASM_LEN]	= {};
					sprintf( tmp_res, 	"JMP	%s\n"
										"LABEL	%s_exit\n\n", while_label, while_label );

					strcat( res_asm_a, tmp_res );

					break;

				}
				default:
				{
					printf( "Unexpected keyword\n" );
					assert( !"Compilation error" );
					break;
				}
			}

			break;
		}
		case CALL:
		{
			char*		call	= 	compile_CALL( root );
			sprintf( res_asm_a, "%s\n", call );
			free( call );
			
			cur_instruction++;

			break;
		}
		default:
		{
			printf( "What?\n" );
			assert( !"Compilation error" );
			break;
		}
	}

	int 		len			= strlen( res_asm_a );
	char*		res_asm		= (char*)calloc( sizeof( char ), len + 1 );

	strcpy( res_asm, res_asm_a );

	return 		res_asm;
}

char*	get_var_rptr( const char*	name, int n )
{
	assert( name );
	variable_t	tvar		= {}; 
	
	strcpy( tvar.name, name );	
	
	int 		pos			= find_by_value( cur_vars, &tvar );
	if( pos == -1 )
	{
		printf( "Undefined reference to %s in func %s\n", name, cur_function->name );
		assert( !"Compilation error" );
	}
	
	lst_get_elem( cur_vars, &tvar, pos );

	char		res_asm_a[MAX_ASM_LEN]	= {};

	sprintf( res_asm_a, "PUSH	r1\n"
						"PUSH	%d\n"
						"ADD\n"
						"POP	r%d", tvar.shift, n );

	int 		len			= strlen( res_asm_a );
	char*		res_asm		= (char*)calloc( sizeof( char ), len + 1 );

	strcpy( res_asm, res_asm_a );

	return 		res_asm;
}

char*	compile_EXP( node_t* root )
{
	assert( root );

	char 		res_asm_a[MAX_ASM_LEN]	= {};

	lex_t		rootlex		= {};
	lex_funcs.copy_func( &rootlex, root->data );

	switch( rootlex.type )
	{
		case VARIABLE:
		{
			int		rsize	= count_node_childs( root );
			if( rsize == 1 )
			{
				char*	var		= get_var_rptr( rootlex.value, 2 );
				sprintf( res_asm_a, "%s\n"
									"PUSH	[r2]", var );

				free( var );
			}
			else
			{
				char*		call	= compile_CALL( root );
				sprintf( res_asm_a, "%s\n"
									"PUSH	r3\n", call );
				free( call );
			}
			break;
		}
		case CONSTANT:
		{
			sprintf( res_asm_a, "PUSH	%lg", *(double*)rootlex.value );
			break;
		}
		case OPERATOR:
		{
			char*	rpart	= compile_EXP( root->right );
			char*	lpart	= compile_EXP( root->left );
			
			char	action[MAX_ASM_LEN]	= {};
			char	op					= rootlex.value[0];

			if     ( op == '+' ) strcpy( action, "ADD" );
			else if( op == '-' ) strcpy( action, "SUB" );
			else if( op == '*' ) strcpy( action, "MUL" );
			else if( op == '/' ) strcpy( action, "DIV" );
			else if( op == '>' ) strcpy( action, "CALL _k_a" );
			else if( op == '<' ) strcpy( action, "CALL _k_b" );
			else if( op == '=' ) strcpy( action, "CALL _k_e" );
			else 
			{
				printf( "Unexpected operator in expression" );
				assert( !"Compilation error" );
			}

			sprintf( res_asm_a, "%s\n"
								"%s\n"
								"%s", lpart, rpart, action );
			free( lpart );
			free( rpart );

			break;
		}
		case KEYWORD:
		{
			keyword_t	keyword	= *(keyword_t*)rootlex.value;
			switch( keyword )
			{
				case SIN:
				{
					char*	lpart	= compile_EXP( root->left );
					sprintf( res_asm_a, "%s\n"
										"SIN", lpart );
					free( lpart );
					break;
				}
				case COS:
				{
					char*	lpart	= compile_EXP( root->left );
					sprintf( res_asm_a, "%s\n"
										"COS", lpart );
					free( lpart );
					break;
				}
				case SQRT:
				{
					char*	lpart	= compile_EXP( root->left );
					sprintf( res_asm_a, "%s\n"
										"SQRT", lpart );
					free( lpart );
					break;
				}
				default:
				{
					assert( !"Compilation error: whaaaaaaaaat!?" );
				}
			}
			break;
		}
		default:
		{
			assert( !"Compilation error: whaaaaaaaaat!?" );
		}
	}

	int 		len			= strlen( res_asm_a );
	char*		res_asm		= (char*)calloc( sizeof( char ), len + 1 );

	strcpy( res_asm, res_asm_a );

	return 		res_asm;
}

char*	compile_CALL( node_t* root )
{
	assert( root );
	
	int 	params_count 	= count_node_childs( root ) - 1;

	function_t		function	= {};
	strcpy( function.name, ((lex_t*)root->data)->value );
	function.params_count 		= params_count;

	int 	pos 	= find_by_value( be_cur_flist, &function );
	if( pos == -1 )
	{
		printf( "Undefined reference to %s with %d params\n", function.name, params_count );
		assert( !"Compilation error" );
	}
		
	for( int i = 0; i < params_count; ++i )
		root = root->left;

	char		res_asm_a[MAX_ASM_LEN] = {};
	sprintf( res_asm_a, "PUSH 	r1\n" );

	for( int i = 0; i < params_count; ++i )
	{
		char*	var		= get_var_rptr(	((lex_t*)root->data)->value, 2 );
		char	tres[MAX_ASM_LEN] = {};
		sprintf( tres, 	"%s\n"
						"PUSH	[r2]\n", var );

		strcat( res_asm_a, tres );
		free( var );
		root = root->parent;
	}
		
	char	tres[MAX_ASM_LEN] 	= {};
	sprintf( tres, 	"PUSH	r1\n"
					"PUSH	%d\n"
					"ADD\n"
					"POP	r1\n"
					"CALL	_%s_f%d\n"
					"POP	r1", cur_max_shift, function.name, params_count );

	strcat( res_asm_a, tres );
	
	int 		len			= strlen( res_asm_a );
	char*		res_asm		= (char*)calloc( sizeof( char ), len + 1 );

	strcpy( res_asm, res_asm_a );

	return 		res_asm;
}

