#include "be.h"

#include <cstdlib>
#include <cassert>
#include <cstdio>

int main( int argc, char* argv[] )
{
	assert( argc == 3 );
	
	FILE*	src_file 	= fopen( argv[1], "r" );
	assert( src_file );
	
	prog_list_t*	prog_list	= read_prog_list( src_file );
	assert( prog_list );
	fclose( src_file );

	char*	asm_code	= compile_asm( prog_list );
	
	FILE*	res_file 	= fopen( argv[2], "w" );
	
	fprintf( res_file, "%s", asm_code );

	fclose( res_file );

	free( asm_code );
	destruct_prog_list( prog_list );

	return 0;
}

