#include "cipl.h"

char*	compile_asm( prog_list_t* prog_list );
char*	compile_function( function_t* function );
char*	compile_instruction( instruction_t*	instruction ); 
