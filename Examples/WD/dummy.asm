_UNIX_P_	equ		0x2000000		; prefix for unix syscalls

extern _printf
extern _scanf

section .data
___fstr: db `%lf\0%.3lf\n\0`

section .text
___IN:
    push    rbp
    mov     rbp, rsp
    sub     rsp, 10h
    and     rsp, -0x10
    mov     rdi, ___fstr; `%lf\0`
    call    _scanf
    mov     rsp, rbp
    pop     rbp
    ret

___OUT:
    push    rbp
    mov     rbp, rsp
    sub     rsp, 10h
    and     rsp, -0x10
    mov     rdi, ___fstr + 04h ; `%.3lf\n\0`
    mov     rax, 01h
    movsd   xmm0, qword [rsi]
    call    _printf
    mov     rsp, rbp
    pop     rbp
    ret

%macro _COSTYL 2
___%1:
    push    rbp
    mov     rbp, rsp

        fld     qword [rbp + 10h]   
        fld     qword [rbp + 18h]
        fcompp
        fstsw   ax
        sahf

        %2     .false

        .true:
        mov     rax, __float64__(1.0)
        jmp .exit
        .false:
        mov     rax, __float64__(0.0)
        .exit:
    pop     rbp
    ret
%endmacro

_COSTYL LESS,    jae
_COSTYL GREATER, jbe
_COSTYL EQUAL,   jne

global _main
_main:
push    rbp
mov     rbp, rsp

    call    ___NOP_ROAD

pop     rbp
xor     rax, rax
ret

___NOP_ROAD:
    dq      0xDEDEDADEDEDAEDAD     
    times   5000000 nop

mov     rax, _UNIX_P_ + 01h
mov     rdi, 01h
syscall

