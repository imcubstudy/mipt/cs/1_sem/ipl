_main_f0:
push   rbp
mov    rbp, rsp
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
sub    rsp, 8d
lea    rsi, [rbp -8d]
call   ___IN
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
sub    rsp, 8d
lea    rsi, [rbp -16d]
call   ___IN
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
sub    rsp, 8d
lea    rsi, [rbp -24d]
call   ___IN
mov    rax, qword [rbp -24d]
push   rax
mov    rax, qword [rbp -16d]
push   rax
mov    rax, qword [rbp -8d]
push   rax
call   _solveSquare_f3
add    rsp, 24d
mov    rsp, rbp
pop    rbp
ret
_solveSquare_f3:
push   rbp
mov    rbp, rsp
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp + 8d]
push   rax
mov    rax, qword [rsp + 8d]
push   rax
call   ___EQUAL
add    rsp, 24d
mov    qword [rsp], rax
fld    qword [rsp]
fldz
fcompp
fstsw  ax
sahf
jz     _endif_154
mov    rax, qword [rbp +32d]
push   rax
mov    rax, qword [rbp +24d]
push   rax
call   _solveLinear_f2
add    rsp, 16d
mov    qword [rsp], rax
mov    rax, qword [rsp]
mov    qword [rbp -8d], rax
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
add    rsp, 0d
_endif_154:
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, __float64__(4.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, qword [rbp +32d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
fld    qword [rsp + 8d]
fsub   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, qword [rbp -16d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp + 8d]
push   rax
mov    rax, qword [rsp + 8d]
push   rax
call   ___LESS
add    rsp, 24d
mov    qword [rsp], rax
fld    qword [rsp]
fldz
fcompp
fstsw  ax
sahf
jz     _endif_247
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp]
mov    qword [rbp -8d], rax
lea    rsi, [rbp -8d]
call   ___OUT
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
add    rsp, 0d
_endif_247:
mov    rax, qword [rbp -16d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp + 8d]
push   rax
mov    rax, qword [rsp + 8d]
push   rax
call   ___EQUAL
add    rsp, 24d
mov    qword [rsp], rax
fld    qword [rsp]
fldz
fcompp
fstsw  ax
sahf
jz     _endif_320
mov    rax, __float64__(1.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp]
mov    qword [rbp -8d], rax
lea    rsi, [rbp -8d]
call   ___OUT
mov    rax, __float64__(-1.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, __float64__(2.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
fld    qword [rsp + 8d]
fdiv   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
lea    rsi, [rbp -24d]
call   ___OUT
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
add    rsp, 8d
_endif_320:
mov    rax, qword [rbp -16d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp + 8d]
push   rax
mov    rax, qword [rsp + 8d]
push   rax
call   ___GREATER
add    rsp, 24d
mov    qword [rsp], rax
fld    qword [rsp]
fldz
fcompp
fstsw  ax
sahf
jz     _endif_440
mov    rax, __float64__(2.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp]
mov    qword [rbp -8d], rax
mov    rax, qword [rbp -16d]
mov    qword [rsp], rax
fld    qword [rsp]
fsqrt
fstp   qword [rsp]
sub    rsp, 8d
mov    rax, __float64__(-1.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, qword [rbp -24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fsub   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, __float64__(2.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
fld    qword [rsp + 8d]
fdiv   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, __float64__(-1.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, qword [rbp -24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fadd   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, __float64__(2.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
fld    qword [rsp + 8d]
fdiv   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
lea    rsi, [rbp -8d]
call   ___OUT
lea    rsi, [rbp -32d]
call   ___OUT
lea    rsi, [rbp -40d]
call   ___OUT
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
add    rsp, 24d
_endif_440:
mov    rsp, rbp
pop    rbp
ret
_solveLinear_f2:
push   rbp
mov    rbp, rsp
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp + 8d]
push   rax
mov    rax, qword [rsp + 8d]
push   rax
call   ___EQUAL
add    rsp, 24d
mov    qword [rsp], rax
fld    qword [rsp]
fldz
fcompp
fstsw  ax
sahf
jz     _endif_674
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
mov    rax, qword [rsp + 8d]
push   rax
mov    rax, qword [rsp + 8d]
push   rax
call   ___EQUAL
add    rsp, 24d
mov    qword [rsp], rax
fld    qword [rsp]
fldz
fcompp
fstsw  ax
sahf
jz     _endif_691
mov    rax, __float64__(1.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fdiv   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
lea    rsi, [rbp -8d]
call   ___OUT
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
add    rsp, 8d
_endif_691:
mov    rax, __float64__(0.00000)
mov    qword [rsp], rax
sub    rsp, 8d
lea    rsi, [rbp -8d]
call   ___OUT
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
add    rsp, 8d
_endif_674:
mov    rax, __float64__(1.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, __float64__(-1.00000)
mov    qword [rsp], rax
sub    rsp, 8d
mov    rax, qword [rbp +24d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fmul   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
mov    rax, qword [rbp +16d]
mov    qword [rsp], rax
fld    qword [rsp + 8d]
fdiv   qword [rsp]
fstp   qword [rsp + 8d]
add    rsp, 8d
sub    rsp, 8d
lea    rsi, [rbp -8d]
call   ___OUT
lea    rsi, [rbp -16d]
call   ___OUT
mov    rax, qword [rbp -8d]
mov    rsp, rbp
pop    rbp
ret
mov    rsp, rbp
pop    rbp
ret

