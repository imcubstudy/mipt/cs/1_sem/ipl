#include "cipl.h"
#include <cassert>
#include <cstdio>
#include <cstdlib>

const data_funcs_t lex_funcs = { print_lex, cmp_lex, copy_lex, destr_lex };

int 	print_lex( FILE* ofstream, const void* lex )
{
	assert( ofstream );
	assert( lex );

	lex_t	l = *(lex_t*)lex;
	switch( l.type )
	{
		case KEYWORD:
		{
			keyword_t keyword = *(keyword_t*)l.value;
			
			fprintf( ofstream, "keyword: " );

			switch( keyword )
			{
				case IF:
				{
					int 	pos = *(int*)(l.value + sizeof( keyword_t ) );
					fprintf( ofstream, "if at %d", pos );
					break;
				}
				case WHILE:
				{
					int 	pos = *(int*)(l.value + sizeof( keyword_t ) );
					fprintf( ofstream, "while at %d", pos );
					break;
				}
				case VAR:
				{
					fprintf( ofstream, "var" );
					break;
				}
				case DEF:
				{
					fprintf( ofstream, "def" );
					break;
				}
				case IN:
				{
					fprintf( ofstream, "in" );
					break;
				}
				case OUT:
				{
					fprintf( ofstream, "out" );
					break;
				}
				case SIN:
				{
					fprintf( ofstream, "sin" );
					break;
				}
				case COS:
				{
					fprintf( ofstream, "cos" );
					break;
				}			
				case SQRT:
				{
					fprintf( ofstream, "sqrt" );
					break;
				}
				case RETURN:
				{
					fprintf( ofstream, "return" );
					break;
				}
			}
			break;
		}
		case BRACKET:
		{
			fprintf( ofstream, "\\\"\\%c\\\"", l.value[0] );
			break;
		}
		case OPERATOR:
		{
			fprintf( ofstream, "\\\"\\%c\\\"", l.value[0] );
			break;
		}
		case VARIABLE:
		{
			fprintf( ofstream, "var: %s", l.value );
			break;
		}
		case CONSTANT:
		{
			fprintf( ofstream, "%lg", *(double*)l.value );
			break;
		}
		case SEP:
		{
			fprintf( ofstream, "\\\"%c\\\"", l.value[0] );
			break;
		}
		case END:
		{
			fprintf( ofstream, "end" );
			break;
		}
	}
	return 1;
}

int		cmp_lex( const void* a, const void* b )
{
	assert( a );
	assert( b );

	lex_t 	la = *(lex_t*)a;
	lex_t	lb = *(lex_t*)b;
	if( la.type != lb.type ) return la.type - lb.type;
	else
	{
		for( int i = 0; i < MAX_VALUE_SIZEOF; ++i )
			if( la.value[i] != lb.value[i] )
				return la.value[i] - lb.value[i];
		return 0;
	}
	return 0;
}

int 	copy_lex( void*	dest, const void* src )
{
	assert( dest );
	assert( src );

	lex_t*	ld 	=  (lex_t*)dest;
	lex_t	ls 	= *(lex_t*)src;

	ld->type 	= ls.type;

	for( int i = 0; i < MAX_VALUE_SIZEOF; ++i )
		ld->value[i] = ls.value[i];
	
	return 0;
}

int 	destr_lex( void* lex )
{
	return 0;
}

const data_funcs_t instruction_type_funcs = { 
	print_instruction_type, 
	int_default_funcs.comparator, 
	int_default_funcs.copy_func, 
	int_default_funcs.destructor };

int 	print_instruction_type( FILE* ofstream, const void* instruction_type )
{
	instruction_type_t 	it = *(instruction_type_t*)instruction_type;

	switch( it )
	{
		case ASSN:
		{
			fprintf( ofstream, "Assn" );
			break;
		}
		case CALL:
		{
			fprintf( ofstream, "Call" );
			break;
		}
		case KW:
		{
			fprintf( ofstream, "Keyword" );
			break;
		}
		case _POISON:
		{
			fprintf( ofstream, "_POISON" );
			break;
		}
	}
	return 1;
}

const data_funcs_t instruction_funcs = {
	print_instruction,
	cmp_instruction,
	copy_instruction,
	destruct_instruction
};

int		print_instruction( FILE* ofstream, const void* instruction )
{
	assert( ofstream );
	assert( instruction );

	instruction_t 	i = *(instruction_t*)instruction;

	fprintf( ofstream, "| { <instruction> " );
	instruction_type_funcs.print_func( ofstream, &i.type );

	fprintf( ofstream, " | <instr_tree> tree \\n %p }", i.instruction_tree );
    return 0;
}

int		cmp_instruction( const void* a, const void* b )
{
	return 1;
}

int 	copy_instruction( void*	dest, const void* src )
{
	assert( dest );
	assert( src );

	instruction_t*	di =  (instruction_t*)dest;
	instruction_t	si = *(instruction_t*)src;

	*di = si;
	di->instruction_tree = copy_tree( si.instruction_tree );

	return 1;
}

int 	destruct_instruction( void*	instruction )
{
	assert( instruction );

	instruction_t i = *(instruction_t*)instruction;

	return destruct_tree( i.instruction_tree );
}

const data_funcs_t function_funcs = {
	print_function,
	cmp_function,
	copy_function,
	destruct_function
};

int		print_function( FILE* ofstream, const void* function )
{
	assert( ofstream );
	assert( function );

	function_t f = *(function_t*)function;
	
	fprintf( ofstream, " | { <fname> function \\n %s | %d params | <instr_list> instr_list %p | <params_list> %p }", f.name, f.params_count, f.instruction_list, f.params_list );

	return 1;
}

int		cmp_function( const void* a, const void* b )
{
	assert( a );
	assert( b );

	function_t fa = *(function_t*)a;
	function_t fb = *(function_t*)b;
	
	int name_cmp = strcmp( fa.name, fb.name );
	if( name_cmp == 0 )
		return fa.params_count - fb.params_count;
	
	return name_cmp;
}

int 	copy_function( void*	dest, const void* src )
{
	assert( dest );
	assert( src );

	function_t* fd =  (function_t*)dest;
	function_t  fs = *(function_t*)src;

	*fd = fs;

	strcpy( fd->name, fs.name );
	fd->instruction_list = copy_list( fs.instruction_list );
	fd->params_list 	 = copy_list( fs.params_list ); 

	return 1;
}

int 	destruct_function( void*	function )
{
	assert( function );

	function_t f = *(function_t*)function;
	
	destruct_list( f.instruction_list );
	destruct_list( f.params_list );
    return 0;
}

#define letso ( sizeof( void* ) + 2 * sizeof( int ) )

int dump_prog_list( FILE* ofstream, prog_list_t* prog_list )
{
	assert( ofstream );
	assert( prog_list );
	assert( lst_ok( prog_list->function_list ) );

	fprintf( ofstream, 	"digraph prog_tree\n"
						"{\n" );

	subdot_list( ofstream, prog_list->function_list, "function_list" );
	
	int num_of_funcs = lst_size( prog_list->function_list );
	for( int i = 0; i < num_of_funcs; ++i )
	{
		function_t*		function = (function_t*)calloc( sizeof( function_t ), 1 );
		
		lst_get_elem( prog_list->function_list, function, i );

		subdot_function( ofstream, function );

		fprintf( ofstream, "Node%p -> Node_%s\n", 
								(char*)(prog_list->function_list->_data) + letso * i,
								function->name 										);

		function_funcs.destructor( function );
		free( function );
	}

	fprintf( ofstream, "\n}\n" );
	return 1;
}

int subdot_function( FILE* ofstream, function_t* function )
{
	assert( ofstream );
	assert( function );

	fprintf( ofstream,  "\nsubgraph cluster_%s\n"
						"{\n"
						"label = \"%s\"\n", function->name, function->name );

	fprintf( ofstream, "\nNode_%s [shape=record, label=\"<params> params %d | <instr_list> instr_list \"]\n", function->name, function->params_count );

	if( lst_size( function->params_list ) != 0 )
	{
		subdot_list( ofstream, function->params_list, "params" );

		fprintf( ofstream, "\nNode_%s:<params> -> InfoNode%p\n", function->name, 
							   									 function->params_list );
	}

	fprintf( ofstream, "\nsubgraph cluster_instrlist\n{\nlabel=\"\"\n" );

	subdot_list( ofstream, function->instruction_list, "instructions" );

	fprintf( ofstream, "\nNode_%s:<instr_list> -> InfoNode%p\n", function->name, 
																function->instruction_list );

	int instruction_count = lst_size( function->instruction_list );
	
	for( int i = 0; i < instruction_count; ++i )
	{	
		instruction_t*	instruction = (instruction_t*)calloc( sizeof( instruction_t ), 1 );
		
		lst_get_elem( function->instruction_list, instruction, i );

		fprintf( ofstream, "\nNode%p [shape=record, label=\"type ", instruction );
		instruction_type_funcs.print_func( ofstream, &instruction->type );
		
		fprintf( ofstream, " | <instr_tree> treeroot %p \"]\n", instruction->instruction_tree->root );

		if( instruction->instruction_tree->root != nullptr )
		{
			fprintf( ofstream, 	"\nsubgraph cluster_instr%p\n"
			"{\nnode [shape=record]\nlabel=\"\"\n", instruction->instruction_tree->root );
		
			dot_node( ofstream, instruction->instruction_tree->root );

			fprintf( ofstream, "\nNode%p:<instr_tree> -> Node%p\n", instruction, instruction->instruction_tree->root );

			fprintf( ofstream, "\n}\n" );
		}

		fprintf( ofstream, "Node%p:<instr_tree> -> Node%p:<instr_tree>\n", 
			(char*)(function->instruction_list->_data) + letso * i, instruction );
	}

	fprintf( ofstream, "\n}\n" );
	fprintf( ofstream, "\n}\n" );
    return 0;
}

int 	destruct_prog_list( prog_list_t* prog_list )
{
	int res 	= destruct_list( prog_list->function_list );
	free( prog_list );

	return res;
}

int	write_prog_list( FILE* ofstream, prog_list_t* prog_list )
{
	assert( ofstream );
	assert( prog_list );

	list_t*		function_list		= 	prog_list->function_list;
	int 		functions_count		=	lst_size( function_list );
	
	fprintf( ofstream, "prog_list\n{\n%d\n", functions_count );

	for( int i = 0; i < functions_count; ++i )
	{
		function_t*		function	= (function_t*)calloc( sizeof( function_t ), 1 );
		lst_get_elem( function_list, function, i );

		write_function( ofstream, function );

		function_funcs.destructor( function );
	}

	fprintf( ofstream, "\n}\n" );

	return 0;
}

prog_list_t*	read_prog_list( FILE* ifstream )
{
	//poison_finction
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	MAKE_LIST( poison_params_list, lex_t, lex_funcs, poison_lex, LST_DEFAULT_CAPACITY ); 

	instruction_t	poison_instruction = { _POISON, poison_instruction_tree };

	MAKE_LIST( poison_instruction_list, instruction_t, instruction_funcs, poison_instruction, 
                                                LST_DEFAULT_CAPACITY );
	function_t 	poison_function = { "$$POISON_FUNC_NAME$$", -1, poison_instruction_list, poison_params_list };

	int 	delt	= 0;
	fscanf( ifstream, "prog_list\n{%n", &delt );
	assert( delt );

	MAKE_LIST( func_list, function_t, function_funcs, poison_function, LST_DEFAULT_CAPACITY );

	int 	fcount = -1;
	fscanf( ifstream, "%d", &fcount );
	assert( fcount != -1 );

	for( int i = 0; i < fcount; ++i )
	{
		int pos 	= 0;
		
		function_t*		func	= read_function( ifstream );
		assert( func );

		lst_push_t( func_list, func, &pos );

		function_funcs.destructor( func );
		free( func );
	}

	prog_list_t*	prog_list 	= (prog_list_t*)calloc( sizeof( prog_list_t ), 1 );
	prog_list->function_list	= func_list;

	return prog_list;
}

//function
int write_function( FILE*	ofstream, function_t*	function )
{
	assert( ofstream );
	assert( function );

	fprintf( ofstream, "function %s\n{\n", function->name );

	int 	params_count = function->params_count;
	
	fprintf( ofstream, "%d params:", params_count );

	list_t*	params_list	 = function->params_list;
	for( int i = 0; i < params_count; ++i )
	{
		lex_t		lex	= { END, {} };
		lst_get_elem( params_list, &lex, i );
		
		fprintf( ofstream, " %s", lex.value );
	}

	fprintf( ofstream, "\n" );

	list_t*		instruction_list	= function->instruction_list;
	int			instruction_count	= lst_size( instruction_list );
	
	fprintf( ofstream, "%d instructions:\n", instruction_count );

	for( int i = 0; i < instruction_count; ++i )
	{
		instruction_t*		instruction	= (instruction_t*)calloc( sizeof( instruction_t ), 1 );

		lst_get_elem( instruction_list, instruction, i );

		write_instruction( ofstream, instruction );
	
		instruction_funcs.destructor( instruction );
		free( instruction );
	}
	
	fprintf( ofstream, "\n}\n" );
	return 0;
}

function_t* read_function( FILE* ifstream )
{
	function_t*	/*poisoned*/	result 	
			= (function_t*)calloc( sizeof( function_t ), 1 );

	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	MAKE_LIST( poison_params_list, lex_t, lex_funcs, poison_lex, LST_DEFAULT_CAPACITY ); 

	instruction_t	poison_instruction = { _POISON, poison_instruction_tree };

	MAKE_LIST( poison_instruction_list, instruction_t, instruction_funcs, poison_instruction, 
                                        LST_DEFAULT_CAPACITY );
	function_t 	poison_function = { "$$POISON_FUNC_NAME$$", -1, poison_instruction_list, poison_params_list };

	*result = poison_function;	
	
	int delt		= 0;
	fscanf( ifstream, "%*[ \n]function %s\n{%n", result->name, &delt );
	if( delt == 0 ) fscanf( ifstream, "function %s\n{%n", result->name, &delt );
	assert( delt );

	int 	fparams 	= -1;
	fscanf( ifstream, "%d params:", &fparams );
	assert( fparams != -1 );

	result->params_count 	= fparams;

	int 	pos 	= 0;
	for( int i = 0; i < result->params_count; ++i )
	{
		lex_t		var		= { VARIABLE, {} };
		fscanf( ifstream, "%s", var.value );

		lst_push_t( result->params_list, &var, &pos );
	}
 
 	int 	finstr 		= -1;
	fscanf( ifstream, "%d instructions:", &finstr );
	assert( finstr != -1 );

	for( int i = 0; i < finstr; ++i )
	{
		instruction_t*	instruction 	= read_instruction( ifstream );
		assert( instruction );
	
		lst_push_t( result->instruction_list, instruction, &pos );

		instruction_funcs.destructor( instruction );
		free( instruction );
	}

	fscanf( ifstream, "%*[ \n]}" );
	return result;
}

//instruction
int	write_instruction( FILE* ofstream, instruction_t* instruction )
{
	assert( ofstream );
	assert( instruction );

	fprintf( ofstream, "instruction %d\n{\n", instruction->type );

	write_instree_root( ofstream, instruction->instruction_tree->root );

	fprintf( ofstream, "\n}\n" );

	return 0;
}

instruction_t*	read_instruction( FILE* ifstream )
{
	instruction_t*		instruction = (instruction_t*)calloc( sizeof( instruction_t ), 1 );

	int delt	= 0;
	fscanf( ifstream, "%*[ \n]instruction %d\n{%n", &instruction->type, &delt );
	if( delt == 0 ) fscanf( ifstream, "instruction %d\n{%n", &instruction->type, &delt );

	lex_t	poison_lex = { END, {} };
	MAKE_TREE( instree, lex_t, lex_funcs, poison_lex );

	set_root( instree, read_instree_root( ifstream ) );

	instruction->instruction_tree = instree;

	fscanf( ifstream, "%*[ \n]}" );
	
	return instruction;
}

//instree_root
int write_instree_root( FILE* ofstream, node_t* root )
{
	assert( ofstream );

	fprintf( ofstream, "[" );
	if( root != nullptr )
	{
		lex_t	data 	= *(lex_t*)root->data;
		fprintf( ofstream, "%d:", data.type );
		switch( data.type )
		{
			case CONSTANT:
			{
				fprintf( ofstream, "%lg", *(double*)data.value );
				break;
			}
			case END:
			{
				fprintf( ofstream, "$" );
				break;
			}
			case KEYWORD:
			{
				keyword_t	keyword = *(keyword_t*)data.value;
				fprintf( ofstream, "%d", keyword );
				if( keyword == IF || keyword == WHILE )
				{
					int pos = *(int*)( data.value + sizeof( keyword_t ) );
					fprintf( ofstream, ":%d", pos );
				}
				break;
			}
			default:
			{
				fprintf( ofstream, "%s", data.value );
				break;
			}
		}
		write_instree_root( ofstream, root->left );
		write_instree_root( ofstream, root->right );
	}
	fprintf( ofstream, "]" );
	return 0;
}

node_t*		read_instree_root( FILE* ifstream )
{

	lex_t	poison_lex = { END, {} };
	MAKE_TREE( res_tree, lex_t, lex_funcs, poison_lex );

	node_t*		result 		= nullptr;
	
	lex_t		res_lex		= poison_lex;
	int 		delt		= 0;
	fscanf( ifstream, "%*[ \n][%d%n", &res_lex.type, &delt );
	if( delt == 0 ) fscanf( ifstream, "[%d%n", &res_lex.type, &delt );
	if( delt != 0 )
	{
		switch( res_lex.type )
		{
			case CONSTANT:
			{
				fscanf( ifstream, ":%lg", res_lex.value );
				break;
			}
			case KEYWORD:
			{
				fscanf( ifstream, ":%d:%d", res_lex.value, res_lex.value + sizeof( int ) );
				break;
			}
			case END:
			{
				fscanf( ifstream, "$" );
				break;
			}
			default:
			{
				fscanf( ifstream, ":%[^[]]", res_lex.value );
				break;
			}
		}
		result 	= construct_node( res_tree, &res_lex );
		set_left ( result, read_instree_root( ifstream ) );
		set_right( result, read_instree_root( ifstream ) );
	}
	fscanf( ifstream, "]" );
	destruct_tree( res_tree );
	return result;
}


// variable_t
int        print_var( FILE* ofstream, const void* variable )
{
    variable_t    var        = *(variable_t*)variable;
    
    fprintf( ofstream, "%s st %d", var.name, var.shift );
    
    return 0;
}

int     copy_var( void* dest, const void* src )
{
    variable_t*        vd    =  (variable_t*)dest;
    variable_t        vs    = *(variable_t*)src;
    
    *vd    = vs;
    strcpy( vd->name, vs.name );
    
    return 0;
}

int        cmp_var( const void* a, const void* b )
{
    variable_t        va    = *(variable_t*)a;
    variable_t        vb    = *(variable_t*)b;
    
    int name_cmp    = strcmp( va.name, vb.name );
    
    return name_cmp;
}

int    destruct_var( void* var )
{
    return 0;
}

const data_funcs_t     var_funcs    = { print_var, cmp_var, copy_var, destruct_var };

