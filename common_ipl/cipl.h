#ifndef		__CIPL_H__
#define		__CIPL_H__

#include "containers.h"
using namespace vl_containers;

#include <cstdio>

#define MAX_VALUE_SIZEOF 1000
#define MAX_ASM_LEN      (1<<14)

enum 	lex_type_t
{
	OPERATOR,
	KEYWORD,
	BRACKET,
	VARIABLE,
	CONSTANT,
	SEP,
	END
};

enum 	keyword_t
{
	IN,
	OUT,
	IF,
	WHILE,
	VAR,
	DEF,
	RETURN,
	SIN,
	COS,
	SQRT
};

struct	lex_t
{
	lex_type_t		type;
	char			value[MAX_VALUE_SIZEOF];
};

extern const data_funcs_t lex_funcs;

int 	print_lex( FILE* ofstream, const void* lex );
int		cmp_lex( const void* a, const void* b );
int 	copy_lex( void*	dest, const void* src );
int 	destr_lex( void* lex );

typedef struct prog_list_t prog_list_t;

int				write_prog_list( FILE* ofstream, prog_list_t* prog_list );
prog_list_t*	read_prog_list(	 FILE* ifstream );

int destruct_prog_list( prog_list_t* prog_list );

enum 	instruction_type_t
{
	_POISON,
	ASSN,
	CALL,
	KW
};

extern const data_funcs_t	istruction_type_funcs;

int 	print_instruction_type( FILE* ofstream, const void* instruction_type );

struct 	instruction_t
{
	instruction_type_t	type;

	tree_t*				instruction_tree;
};

extern const data_funcs_t instruction_funcs;

int		print_instruction( FILE* ofstream, const void* instruction );
int		cmp_instruction( const void* a, const void* b );
int 	copy_instruction( void*	dest, const void* src );
int 	destruct_instruction( void*	instruction );

int	write_instruction( FILE* ofstream, instruction_t* instruction );
instruction_t* read_instruction( FILE* ifstream );

int write_instree_root( FILE* ofstream, node_t* root );
node_t*	read_instree_root( FILE* ifstream );

struct 	function_t
{
	char		name[MAX_VALUE_SIZEOF];
	int			params_count;

	list_t*		instruction_list;
	list_t*		params_list;
};

extern const data_funcs_t function_funcs;

int		print_function( FILE* ofstream, const void* function );
int		cmp_function( const void* a, const void* b );
int 	copy_function( void*	dest, const void* src );
int 	destruct_function( void*	function );

int write_function( FILE* ofstream, function_t* function );
function_t* read_function( FILE* ifstream );

struct 	prog_list_t
{
	list_t*		function_list;
};

int dump_prog_list( FILE* ofstream, prog_list_t* prog_list );
int subdot_function( FILE* ofstream, function_t* function );


struct     variable_t
{
    char    name[MAX_ASM_LEN]    = {};
    int     shift                = -1;
};

int    print_var( FILE* ofstream, const void* variable );

int    copy_var( void* dest, const void* src );

int    cmp_var( const void* a, const void* b );

int    destruct_var( void* var );

extern const data_funcs_t     var_funcs;

#endif // __CIPL_H__

