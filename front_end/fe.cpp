#include "cipl.h"
#include <cstdio>
#include <cassert>
#include <cstdlib>

size_t 		file_size( FILE* file )
{
	assert( file );
	assert( ferror( file ) == 0 );

	size_t 		curpos 	= ftell( file );

	fseek( file, 0, SEEK_END );
	size_t 		size 	= ftell( file );

	fseek( file, curpos, SEEK_SET );

	return size;
}

char* 		get_src( char	src_filename[] )
{
	assert( src_filename );

	FILE*	fin 	= fopen( src_filename, "r" );

	size_t 	size 	= file_size( fin );

	char* 	buf		= (char*)calloc( sizeof( char ), size + 1 );
	assert( buf != nullptr );

	fread( buf, sizeof( char ), size, fin );
	
	fclose( fin );

	return buf;
}

#define 	CHECK_CL_FOR( lexstr )								\
	strstr( src_buf + curpos, lexstr ) == src_buf + curpos

#define		CHECK_CL_FOR_SYMB( symb, Type )						\
	if( src_buf[curpos] == symb )								\
	{															\
		lex[curlex].type 		= Type;							\
		lex[curlex].value[0]	= symb;							\
		curlex++;												\
		found = true;											\
		curpos += 1;											\
	}

#define 	CHECK_CL_FOR_ST( symbs, type )						\
	if( true )													\
	{															\
		int size 	= strlen( symbs );							\
		for( int i = 0; i < size; ++i )							\
		{														\
			CHECK_CL_FOR_SYMB( symbs[i], type );				\
		}														\
	}

#define		TRY_CHECK( str, val )										\
	if( CHECK_CL_FOR( str ) )											\
	{																	\
		lex[curlex].type									= KEYWORD;	\
		*(int*)lex[curlex].value							= val;		\
		found												= true;		\
																		\
		curlex++;														\
		curpos += strlen( str );										\
	}

lex_t*		get_lex( char*	src_buf )
{
	assert(	src_buf );

	int len = strlen( src_buf );

	lex_t*	lex 	= (lex_t*)calloc( sizeof( lex_t ), len );
	
	int 	curline = 0;
	int 	curpos 	= 0;
	int 	curlex 	= 0;
	while( curpos < len )
	{
		while( curpos < len )
		{
			if( strchr( " \t", src_buf[curpos] ) != nullptr ) 
				curpos++;
			else if( src_buf[curpos] == '\n' )
				curline++, curpos++;
			else break;
		}
		if( curpos == len ) break;

		bool 	found 	= false; 

		if( CHECK_CL_FOR( "if" ) )
		{
			lex[curlex].type									= KEYWORD;
			*(int*)lex[curlex].value							= IF;
			*(int*)(lex[curlex].value + sizeof( keyword_t ) ) 	= curpos;
			found 												= true;

			curlex++;
			curpos += strlen( "if" );
		}
		else if( CHECK_CL_FOR( "while" ) )
		{
			lex[curlex].type									= KEYWORD;
			*(int*)lex[curlex].value							= WHILE;
			*(int*)(lex[curlex].value + sizeof( keyword_t ) ) 	= curpos;
			found												= true;
			
			curlex++;
			curpos += strlen( "while" );
		}
		else TRY_CHECK( "var", VAR )	
		else TRY_CHECK( "def", DEF )	
		else TRY_CHECK( "in", IN )	
		else TRY_CHECK( "out", OUT )	
		else TRY_CHECK( "return", RETURN )
		else TRY_CHECK( "sin", SIN )
		else TRY_CHECK( "cos", COS )
		else TRY_CHECK( "sqrt", SQRT )
		else
		{
			CHECK_CL_FOR_ST( "+-/*=><!", OPERATOR )
			CHECK_CL_FOR_ST( "{}()[]", 	 BRACKET  )
			CHECK_CL_FOR_ST( ",;", SEP )
		}

		if( !found )
		{
			if( src_buf[curpos] >= '0' && src_buf[curpos] <= '9' )
			{
				lex[curlex].type 									= CONSTANT;
				int 	delt 										= 0;
				sscanf( src_buf + curpos, "%lg%n", lex[curlex].value, &delt );

				curlex++;
				curpos += delt;
			}
			else
			{
				lex[curlex].type 									= VARIABLE;
				int 	delt 										= 0;
				sscanf( src_buf + curpos, "%[^ (){},;\n\t+-*/=><!]%n", 
								lex[curlex].value, &delt );
			
				if( delt == 0 ) break;

				curlex++;
				curpos += delt;
			}
		}	
	}
	
	lex[curlex].type = END;

	return lex;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////						GRAMMAR									/////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

#define errmsg( msg )							\
	{											\
		success = false;						\
		printf( msg " at lex %d\n", curpos );	\
		assert( !"err" );						\
	}

//////////////////////////////////
list_t*		fe_cur_flist 	= nullptr;
int			curpos		    = 0;
lex_t*		curlex		    = nullptr;
int			brb			    = 0;
//////////////////////////////////

function_t*		getF();
instruction_t*	getI();
tree_t*			getCALL();
tree_t*			getKW();
tree_t*			getASSN();
tree_t*			getCOMP();
tree_t*			getEXP();
tree_t*			getEXPT();
tree_t*			getEXPP();
node_t*			getEXPN();

/////////////////////////////////////////////////////////////////////////////////////////////////////

function_t*		getF()
{
	bool success = true;
	
	function_t*	/*poisoned*/	result 	
			= (function_t*)calloc( sizeof( function_t ), 1 );

	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	MAKE_LIST( poison_params_list, lex_t, lex_funcs, poison_lex, LST_DEFAULT_CAPACITY ); 

	instruction_t	poison_instruction = { _POISON, poison_instruction_tree };

	MAKE_LIST( poison_instruction_list, instruction_t, instruction_funcs, poison_instruction, 
																						LST_DEFAULT_CAPACITY ); 
	function_t 	poison_function = { "$$POISON_FUNC_NAME$$", -1, poison_instruction_list, poison_params_list };

	*result = poison_function;

	brb 	= 0;

	if( curlex[curpos].type == KEYWORD )
	{
		keyword_t	keyword = *(keyword_t*)curlex[curpos].value;
		if( keyword == DEF )
		{
			curpos++;
			if( curlex[curpos].type == VARIABLE )
			{
				strcpy( result->name, curlex[curpos].value );
				curpos++;
				
				if( curlex[curpos].type == BRACKET )
				{
					if( curlex[curpos].value[0] == '(' )
					{
						curpos++;

						bool params_got = false;

						while( curlex[curpos].type == VARIABLE )
						{
							int pos = 0;
							lst_push_t( result->params_list, &curlex[curpos], &pos );
							curpos++;
							
							params_got = true;

							if( curlex[curpos].type == SEP )
							{
								if( curlex[curpos].value[0] == ',' )
								{
									curpos++;
								}
								else if( !params_got ) errmsg( "Expected \',\'" );
							}
							else if( !params_got ) errmsg( "Expected separator" )
						}
						
						result->params_count = lst_size( result->params_list );

						if( curlex[curpos].type == BRACKET )
						{
							if( curlex[curpos].value[0] == ')' )
							{
								curpos++;
								
								if( curlex[curpos].type == BRACKET )
								{
									if( curlex[curpos].value[0] == '{' )
									{	
										curpos++;

										bool 	instruction_got = false;
										while( curlex[curpos].value[0] != '}' )
										{
											instruction_t*	instruction = getI();
											if( instruction != nullptr )
											{
												int pos = 0;
												instruction_got = true;
												lst_push_t( result->instruction_list, instruction, &pos );

												instruction_funcs.destructor( instruction );
												free( instruction );
											}
										
											while( brb > 0 && curlex[curpos].value[0] == '}' )
												curpos++, brb--;

										}
										if( instruction_got )
										{
											if( curlex[curpos].type == BRACKET )
											{
												if( curlex[curpos].value[0] == '}' )
												{
													curpos++;
												}
												else errmsg( "Expected \'}\'" )
											}
											else errmsg( "Expected bracket" )
										}
										else errmsg( "No instructions in func" )
									}
									else errmsg( "Expected \'{\'" )
								}
								else errmsg( "Expected bracket" )
							}
							else errmsg( "Expected \')\'" )
						}
						else errmsg( "Expected bracket" )
					}
					else errmsg( "Expected \'(\'" )
				}
				else errmsg( "Expected bracket" )
			}
			else errmsg( "Expected function name" )
		}
		else errmsg( "Expected \'def\'" )
	}
	else errmsg( "Expected keyword" )

	if( find_by_value( fe_cur_flist, result ) != -1 )
	{
		errmsg( "Double function declaration" );
	}

	brb 	= 0;

	if( success )
	{
		printf( "Got function %s\n", result->name );
		return result;
	}
	else 
	{
		function_funcs.destructor( result );
		free( result );
		result = nullptr;

		return result;
	}
}

instruction_t*	getI()
{
	instruction_t* /*poisoned*/	result 
			= (instruction_t*)calloc( sizeof( instruction_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	MAKE_LIST( poison_params_list, lex_t, lex_funcs, poison_lex, LST_DEFAULT_CAPACITY ); 

	instruction_t	poison_instruction = { _POISON, poison_instruction_tree };
	*result = poison_instruction;

	bool 	success 	= true;
	
	switch( curlex[curpos].type )
	{
		case KEYWORD:
		{
			tree_t*		instr_tree = getKW();
			if( instr_tree != nullptr )
			{
				destruct_tree( result->instruction_tree );
				result->instruction_tree = instr_tree;
				result->type = KW;
			}
			else errmsg( "Keyword syntax error" )
			break;
		}
		case VARIABLE:
		{
			if( curlex[curpos + 1].type == BRACKET )
			{
				tree_t*		instr_tree = getCALL();
				if( instr_tree != nullptr )
				{
					destruct_tree( result->instruction_tree );
					result->instruction_tree = instr_tree;
					result->type = CALL;

					if( curlex[curpos].type == SEP )
					{
						if( curlex[curpos].value[0] == ';' )
						{
							curpos++;
						}
						else errmsg( "Expected \';\'" ) 
					}
					else errmsg( "Expected sep" ) 
				}
				else errmsg( "Call syntax error" )
			}
			else 
			{
				tree_t*		instr_tree = getASSN();
				if( instr_tree != nullptr )
				{
					destruct_tree( result->instruction_tree );
					result->instruction_tree = instr_tree;
					result->type = ASSN;
					
					if( curlex[curpos].type == SEP )
					{
						if( curlex[curpos].value[0] == ';' )
						{
							curpos++;
						}
						else errmsg( "Expected \';\'" ) 
					}
					else errmsg( "Expected sep" ) 
				}
				else errmsg( "Assn syntax error" )
			}
			break;
		}
		default: errmsg( "Expected instruction" )
	}
	
	if( success )
	{
		return result;
	}
	else
	{
		instruction_funcs.destructor( result );
		free( result );

		return nullptr;
	}
}

tree_t*	getASSN()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;

	node_t*		res_root 	= nullptr;
	bool 		success		= true;

	if( curlex[curpos].type == VARIABLE )
	{
		res_root = construct_node( result, &curlex[curpos] );
		curpos++;

		if( curlex[curpos].type == OPERATOR )
		{
			if( curlex[curpos].value[0] == '=' )
			{
				node_t*		tnode 	 = construct_node( result, &curlex[curpos] );
				set_left( tnode, res_root );
				
				res_root 			 = tnode;
				
				curpos++;
				
				tree_t*	 	exp_tree = getCOMP();
				if( exp_tree != nullptr )
				{
					set_right( res_root, exp_tree->root );
					exp_tree->root = nullptr;

					destruct_tree( exp_tree );
				}
				else errmsg( "Can't get exp" )
			}
			else errmsg( "Expected \'=\'" )
		}
		else errmsg( "Expected operator" )
	}
	else errmsg( "Expected variable" )

	if( success )
	{
		set_root( result, res_root );
		return result;
	}
	else
	{
		destruct_tree( result );
		destruct_node( res_root );
		return nullptr;
	}
}

tree_t*	getCOMP()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;
	
	bool 	success 	 = true;
	
	node_t*		res_root = nullptr;

	tree_t*		subtree = getEXP();
	if( subtree != nullptr )
	{
		res_root 		= subtree->root;
		subtree->root 	= nullptr;
		destruct_tree( subtree );

		bool gotE 	= true;
		while( gotE && success && curlex[curpos].type == OPERATOR )
		{
			node_t*		tnode 	= nullptr;
			switch( curlex[curpos].value[0] )
			{
				case '>':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;

					subtree = getEXP();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				case '<':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;
					
					subtree = getEXP();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				case '=':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;
					
					subtree = getEXP();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				default:
				{
					gotE = false;
				}
			}
		}
	}
	else errmsg( "Exptected expression" )

	if( success )
	{
		set_root( result, res_root );
		return result;
	}
	else
	{
		destruct_tree( result );
		destruct_node( res_root );
		
		return nullptr;
	}

}

tree_t*	getEXP()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;
	
	bool 	success 	 = true;
	
	node_t*		res_root = nullptr;

	tree_t*		subtree = getEXPT();
	if( subtree != nullptr )
	{
		res_root 		= subtree->root;
		subtree->root 	= nullptr;
		destruct_tree( subtree );

		bool gotT 	= true;
		while( gotT && success && curlex[curpos].type == OPERATOR )
		{
			node_t*		tnode 	= nullptr;
			switch( curlex[curpos].value[0] )
			{
				case '+':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;

					subtree = getEXPT();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				case '-':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;
					
					subtree = getEXPT();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				default:
				{
					gotT = false;
				}
			}
		}
	}
	else errmsg( "Exptected expression" )

	if( success )
	{
		set_root( result, res_root );
		return result;
	}
	else
	{
		destruct_tree( result );
		destruct_node( res_root );
		
		return nullptr;
	}
}

tree_t* getEXPT()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;
	
	bool 	success 	 = true;
	
	node_t*		res_root = nullptr;

	tree_t*		subtree = getEXPP();
	if( subtree != nullptr )
	{
		res_root 		= subtree->root;
		subtree->root 	= nullptr;
		destruct_tree( subtree );
		
		bool gotP 	= true;
		while( gotP && success && curlex[curpos].type == OPERATOR )
		{
			node_t*		tnode 	= nullptr;
			switch( curlex[curpos].value[0] )
			{
				case '/':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;

					subtree = getEXPP();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				case '*':
				{
					tnode 	= construct_node( result, &curlex[curpos] );
					curpos++;  
					
					set_left( tnode, res_root );
					res_root = tnode;
					
					subtree = getEXPP();
					if( subtree != nullptr )
					{
						set_right( res_root, subtree->root );
						subtree->root = nullptr;
						destruct_tree( subtree );
					}
					else errmsg( "Expected expression" )

					break;
				}
				default: 
				{
					gotP = false;
				}
			}
		}
	}
	else errmsg( "Exptected expression" )

	if( success )
	{
		set_root( result, res_root );
		return result;
	}
	else
	{
		destruct_tree( result );
		destruct_node( res_root );
		
		return nullptr;
	}
	
}

tree_t*	getEXPP()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;

	bool 		success 	= true;
	tree_t*		subtree 	= nullptr;
	node_t*		res_root 	= nullptr;

	switch( curlex[curpos].type )
	{
		case BRACKET:
		{
			if( curlex[curpos].value[0] == '(' )
			{
				curpos++;
				
				subtree 	= getCOMP();
				if( subtree != nullptr )
				{
					if( curlex[curpos].type == BRACKET )
					{
						if( curlex[curpos].value[0] == ')' )
						{
							res_root 		= subtree->root;
							subtree->root 	= nullptr;
							destruct_tree( subtree );
								
							curpos++;
						}
						else errmsg( "Expected \')\'" )
					} 
					else errmsg( "Expected bracket" )
				}
				else errmsg( "Expected expression" )
			}
			else errmsg( "Expected \'(\'")

			break;
		}
		case VARIABLE:
		{
			if( curlex[curpos + 1].type == BRACKET && curlex[curpos + 1].value[0] == '(' )
			{
				subtree = getCALL();
				if( subtree != nullptr )
				{
					res_root 		= subtree->root;
					subtree->root 	= nullptr;
					destruct_tree( subtree );
				}
				else errmsg( "Can't get call" )
			}
			else 
			{
				res_root 	= construct_node( result, &curlex[curpos] );
				curpos++;
			}

			break;
		}
		case OPERATOR:
		{
			if( curlex[curpos + 1].type == CONSTANT )
			{
				res_root	= getEXPN();
				if( res_root == nullptr ) errmsg( "Can't get constant" )
			}
			else errmsg( "Unexpected operator" )

			break;
		}
		case CONSTANT:
		{
			res_root = getEXPN();
			if( res_root == nullptr ) errmsg( "Can't get constant" )

			break;
		}
		case KEYWORD:
		{
			
			keyword_t	kw = *(keyword_t*)curlex[curpos].value;

			if( !(kw >= SIN && kw <= SQRT) ) errmsg( "Unexpected keyword" )

			res_root 	= construct_node( result, &curlex[curpos] );
			curpos++;

			if( curlex[curpos].type == BRACKET && curlex[curpos].value[0] == '(' )
			{
				curpos++;
				
				subtree = getEXP();
				if( subtree != nullptr )
				{
					set_left( res_root, subtree->root );
					subtree->root 	= nullptr;
					destruct_tree( subtree );
					if( curlex[curpos].type == BRACKET && curlex[curpos].value[0] == ')' )
						curpos++;
					else errmsg( "Expected \')\'" )
				}
				else errmsg( "Can't get call" )
			} 
			else errmsg( "Expected \'(\'" )

			break;
		}
		default: errmsg( "Unexpected lex" );
	}

	if( success )
	{
		set_root( result, res_root );
		
		return result;
	}
	else 
	{
		destruct_tree( result );
		destruct_node( res_root );

		return nullptr;
	}
}

node_t*		getEXPN()
{
	tree_t* /*poisoned*/	resulttree 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*resulttree 	= *poison_instruction_tree;

	bool 	    success = true;

	node_t*		result  = nullptr;

	if( curlex[curpos].type == CONSTANT )
	{
		result = construct_node( resulttree, &curlex[curpos] );
		curpos++;
	}
	else if( curlex[curpos].type == OPERATOR )
	{
		if( curlex[curpos].value[0] == '-' )
		{
			lex_t 	lex = { END, {} };
			curpos++;
			if( curlex[curpos].type == CONSTANT )
			{
				lex_funcs.copy_func( &lex, &curlex[curpos] );

				double value 		= *(double*)lex.value;
				value 			   *= -1;
				*(double*)lex.value = value;
			
				result = construct_node( resulttree, &lex );
				curpos++;
			}
			else errmsg( "Expected constant" )
		}
		else if( curlex[curpos].value[0] == '+' )
		{
			curpos++;
			
			if( curlex[curpos].type == CONSTANT )
			{
				result = construct_node( resulttree, &curlex[curpos] );
				curpos++;
			}
			else errmsg( "Expected constant" )
		}
		else errmsg( "Unexpected operator" )
	}

	if( success )
	{
		destruct_tree( resulttree );
		
		return result;
	}
	else 
	{
		destruct_tree( resulttree );
		destruct_node( result );

		return nullptr;
	}
}

tree_t*	getCALL()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;

	bool 		success 	= true;
	
	node_t*		res_root 	= nullptr;

	if( curlex[curpos].type == VARIABLE )
	{
		res_root = construct_node( result, &curlex[curpos] );
		curpos++;

		if( curlex[curpos].type == BRACKET )
		{
			if( curlex[curpos].value[0] == '(' )
			{
				curpos++;

				node_t*			lst_end		= res_root;
				
				if( curlex[curpos].type == VARIABLE )
				{
					node_t*		tmp_node 	= construct_node(result, &curlex[curpos] );
					set_left( lst_end, tmp_node );
					lst_end 				= tmp_node;

					curpos++;

					bool 	got_param 	= true;
					while( got_param )
					{
						if( curlex[curpos].type == SEP )
						{
							if( curlex[curpos].value[0] == ',' )
							{
								curpos++;

								if( curlex[curpos].type == VARIABLE )
								{
									node_t*		tmp_node 	= construct_node( result, &curlex[curpos] );
									set_left( lst_end, tmp_node );
									lst_end 				= tmp_node;

									curpos++;
								}
								else errmsg( "Expected variable" )
							}
							else errmsg( "Expected \',\'" )
						}
						else got_param = false;
					}
				}
				if( curlex[curpos].type == BRACKET )
				{
					if( curlex[curpos].value[0] == ')' )
					{
						curpos++;
					}
					else errmsg( "Expected \')\'" )
				}
				else errmsg( "Expected bracket" )
			}
			else errmsg( "Expected \'(\'" )
		}
		else errmsg( "Expected bracket" )
	}
	else errmsg( "Expected function name" )

	if( success )
	{
		set_root( result, res_root );

		return result;
	}
	else 
	{
		destruct_tree( result );
		destruct_node( res_root );

		return nullptr;
	}
	return nullptr;
}

tree_t*		getKW()
{
	tree_t* /*poisoned*/	result 
			= (tree_t*)calloc( sizeof( tree_t ), 1 );
	
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	
	*result 	= *poison_instruction_tree;

	bool		success	 = true;
	node_t*		res_root = nullptr;

	if( curlex[curpos].type == KEYWORD )
	{
		keyword_t	keyword 	= *(keyword_t*)curlex[curpos].value;

		switch( keyword )
		{
			case IN:
			{
				res_root 	= construct_node( result, &curlex[curpos] );
				curpos++;

				if( curlex[curpos].type == BRACKET )
				{
					if( curlex[curpos].value[0] == '(' )
					{
						curpos++;
						if( curlex[curpos].type == VARIABLE )
						{
							node_t*		tmp 	= construct_node( result, &curlex[curpos] );
							set_left( res_root, tmp );
							curpos++;
							
							if( curlex[curpos].type == BRACKET )
							{
								if( curlex[curpos].value[0] == ')' )
								{
									curpos++;
									if( curlex[curpos].type == SEP )
									{
										if( curlex[curpos].value[0] == ';' )
										{
											curpos++;
										}
										else errmsg( "Expected \';\'" )
									}
									else errmsg( "Expected sep" )
								}
								else errmsg( "Expected \')\'" )
							}
							else errmsg( "Expected bracket" )
						}
						else errmsg( "Expected variable" )
					}
					else errmsg( "Expected \'(\'" )
				}
				else errmsg( "Expected bracket" )
				
				break;
			}
			case OUT:
			{
				res_root 	= construct_node( result, &curlex[curpos] );
				curpos++;

				if( curlex[curpos].type == BRACKET )
				{
					if( curlex[curpos].value[0] == '(' )
					{
						curpos++;
						if( curlex[curpos].type == VARIABLE )
						{
							node_t*		tmp 	= construct_node( result, &curlex[curpos] );
							set_left( res_root, tmp );
							curpos++;
							
							if( curlex[curpos].type == BRACKET )
							{
								if( curlex[curpos].value[0] == ')' )
								{
									curpos++;
									if( curlex[curpos].type == SEP )
									{
										if( curlex[curpos].value[0] == ';' )
										{
											curpos++;
										}
										else errmsg( "Expected \';\'" )
									}
									else errmsg( "Expected sep" )
								}
								else errmsg( "Expected \')\'" )
							}
							else errmsg( "Expected bracket" )
						}
						else errmsg( "Expected variable" )
					}
					else errmsg( "Expected \'(\'" )
				}
				else errmsg( "Expected bracket" )
				
				break;
			}
			case IF:
			{
				res_root	= construct_node( result, &curlex[curpos] );
				curpos++;

				int sbrb 	= brb;

				if( curlex[curpos].type == BRACKET )
				{
					if( curlex[curpos].value[0] == '(' )
					{
						curpos++;
						
						tree_t*		exp_tree 	= getCOMP();

						if( exp_tree != nullptr )
						{
							node_t*		tmp 	= exp_tree->root;
							exp_tree->root		= nullptr;
							destruct_tree( exp_tree );

							set_left( res_root, tmp );
							
							if( curlex[curpos].type == BRACKET )
							{
								if( curlex[curpos].value[0] == ')' )
								{
									curpos++;
									
									if( curlex[curpos].type == BRACKET )
									{
										if( curlex[curpos].value[0] == '{' )
										{
											curpos++;
											int 	instruction_count = 0;
											
											int		startkostylpos 	= curpos;
											
											instruction_t*	instr	= getI();
											if( instr != nullptr )
											{
												bool gotI	= true;
												while( success && gotI )
												{
													instruction_count++;
													instruction_funcs.destructor( instr );
													free( instr );
													instr = nullptr;

													while( brb > sbrb && curlex[curpos].value[0] == '}' )
														curpos++, brb--;
													
													if( curlex[curpos].value[0] != '}' ) 
														instr 	= getI();

													if( instr	== nullptr ) gotI = false;
												}
												if( curlex[curpos].type == BRACKET )
												{
													if( curlex[curpos].value[0] == '}' )
													{
														curpos	= startkostylpos;
														lex_t	kostyl_res = { CONSTANT, {} };
														*(double*)kostyl_res.value 	= (double)instruction_count;

														brb = sbrb + 1;

														node_t*		tmp = construct_node( result, &kostyl_res );
														set_right( res_root, tmp );
													}
													else errmsg( "Expected \'}\'" )
												}
												else errmsg( "Expected bracket" )
											}
											else errmsg( "Can't check instructions" )
										}
										else errmsg( "Expected \'{\'" )
									}
									else errmsg( "Expected bracket" )
								}
								else errmsg( "Expected \')\'" )
							}
							else errmsg( "Expected bracket" )
						}
						else errmsg( "Expected expression" )
					}
					else errmsg( "Expected \'(\'" )
				}
				else errmsg( "Expected bracket" )

				break;
			}
			case WHILE:
			{
				res_root	= construct_node( result, &curlex[curpos] );
				curpos++;
				
				int		sbrb	= brb;

				if( curlex[curpos].type == BRACKET )
				{
					if( curlex[curpos].value[0] == '(' )
					{
						curpos++;
						
						tree_t*		exp_tree 	= getCOMP();

						if( exp_tree != nullptr )
						{
							node_t*		tmp 	= exp_tree->root;
							exp_tree->root		= nullptr;
							destruct_tree( exp_tree );

							set_left( res_root, tmp );
							
							if( curlex[curpos].type == BRACKET )
							{
								if( curlex[curpos].value[0] == ')' )
								{
									curpos++;
									
									if( curlex[curpos].type == BRACKET )
									{
										if( curlex[curpos].value[0] == '{' )
										{
											curpos++;
											int 	instruction_count = 0;
											
											int		startkostylpos 	= curpos;
											
											instruction_t*	instr	= getI();
											if( instr != nullptr )
											{
												bool gotI	= true;
												while( success && gotI )
												{
													instruction_count++;
													instruction_funcs.destructor( instr );
													free( instr );
													instr = nullptr;
													
													while( brb > sbrb && curlex[curpos].value[0] == '}' )
														curpos++, brb--;
													
													if( curlex[curpos].value[0] != '}' )
														instr 	= getI();
																											
													if( instr	== nullptr ) gotI = false;
												}
												if( curlex[curpos].type == BRACKET )
												{
													if( curlex[curpos].value[0] == '}' )
													{
														curpos	= startkostylpos;
														
														brb		= sbrb + 1;

														lex_t	kostyl_res = { CONSTANT, {} };
														*(double*)kostyl_res.value 	= (double)instruction_count;

														node_t*		tmp = construct_node( result, &kostyl_res );
														set_right( res_root, tmp );
													}
													else errmsg( "Expected \'}\'" )
												}
												else errmsg( "Expected bracket" )
											}
											else errmsg( "Can't check instructions" )
										}
										else errmsg( "Expected \'{\'" )
									}
									else errmsg( "Expected bracket" )
								}
								else errmsg( "Expected \')\'" )
							}
							else errmsg( "Expected bracket" )
						}
						else errmsg( "Expected expression" )
					}
					else errmsg( "Expected \'(\'" )
				}
				else errmsg( "Expected bracket" )

				break;
			}
			case VAR:
			{
				res_root 	=	construct_node( result, &curlex[curpos] );
				curpos++;
				
				if( curlex[curpos].type == VARIABLE )
				{
					node_t*		tnode	= construct_node( result, &curlex[curpos] );
					set_left( res_root, tnode );

					curpos++;
					
					if( curlex[curpos].type == OPERATOR && curlex[curpos].value[0] == '=' )
					{
						curpos++;

						tree_t*		exp_tree 	= getCOMP();
						if( exp_tree != nullptr )
						{
							tnode 	= exp_tree->root;
							exp_tree->root = nullptr;
							destruct_tree( exp_tree );

							set_right( res_root, tnode );

							if( curlex[curpos].type == SEP && curlex[curpos].value[0] == ';' )
							{
								curpos++;
							}
							else errmsg( "Expected \';\'" )
						}
						else errmsg( "Can't get expression" )
					}
					else errmsg( "Expected \'=\'" )
				}
				else errmsg( "Expected variable name" )
				
				break;
			}
			case RETURN:
			{
				res_root = construct_node( result, &curlex[curpos] );
				curpos++;
				
				if( curlex[curpos].type == VARIABLE )
				{
					node_t*		tnode	= construct_node( result, &curlex[curpos] );
					set_left( res_root, tnode );
					
					curpos++;
					
					if(	curlex[curpos].type == SEP && curlex[curpos].value[0] == ';' )
					{
						curpos++;
					}
					else errmsg( "Expected \';\'" )
				}
				else errmsg( "Expected variable" )

				break;
			}
			default: errmsg( "Unexpected kw" );
		}
	}
	else errmsg( "Expected keyword" )

	if( success )
	{
		set_root( result, res_root );
		
		return result;
	}
	else 
	{
		destruct_tree( result );
		destruct_node( res_root );

		return nullptr;
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////						GRAMMAR									/////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////


prog_list_t*	compile_prog_list( lex_t* lex )
{
	assert( lex );

	// creation of poisoned function list 
	lex_t	poison_lex = { END, {} };

	MAKE_TREE( poison_instruction_tree, lex_t, lex_funcs, poison_lex );
	MAKE_LIST( poison_params_list, lex_t, lex_funcs, poison_lex, LST_DEFAULT_CAPACITY ); 

	instruction_t	poison_instruction = { _POISON, poison_instruction_tree };

	MAKE_LIST( poison_instruction_list, instruction_t, instruction_funcs, poison_instruction, 
																						LST_DEFAULT_CAPACITY ); 
	function_t 	poison_function = { "$$POISON_FUNC_NAME$$", -1, poison_instruction_list, poison_params_list };

	MAKE_LIST( function_list, function_t, function_funcs, poison_function, LST_DEFAULT_CAPACITY );

	prog_list_t*	prog_list 	= (prog_list_t*)calloc( sizeof( prog_list ), 1 );
	prog_list->function_list 	= function_list;

	fe_cur_flist    = function_list;
	curpos 			= 0;
	curlex 			= lex;

	bool 	success = false;
	
	bool 	gotF 	= true;
	while( gotF && curlex[curpos].type != END )
	{
		function_t* 	function = getF();
			
		if( function != nullptr ) 
		{
			success = true;
			
			int pos = 0;
			assert( lst_push_t( function_list, function, &pos ) == NO_ERROR );

			function_funcs.destructor( function );
			free( function );
		}
		else gotF = false;
	}

	fe_cur_flist    = nullptr;
	curpos 			= 0;
	curlex 			= nullptr;

	if(	success )
		return prog_list;
	else
	{
		destruct_list( function_list );
		free( prog_list );

		return nullptr;
	}
}










