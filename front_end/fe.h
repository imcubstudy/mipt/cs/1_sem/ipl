#include <cstdio>
#include "cipl.h"

char*			get_src( char 	src_filename[] );
lex_t*			get_lex( char*	src_buf );

prog_list_t*	compile_prog_list( lex_t* lex );

size_t 			file_size( FILE* file );

