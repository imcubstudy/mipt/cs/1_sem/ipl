#include "fe.h"
#include <cstdio>
#include <cassert>
#include <cstdlib>

int main( int argc, char* argv[] )
{
	assert( argc == 3 );

	char*			src 		= get_src( argv[1] );

	lex_t*			lex 		= get_lex( src );

	printf( "0: " ); lex_funcs.print_func( stdout, &lex[0] );
	printf( "\n" );

	for( int i = 1; lex[i -1].type != END; ++i )
	{
		printf( "%d: ", i ); lex_funcs.print_func( stdout, &lex[i] );
		printf( "\n" );
	}

	prog_list_t*	prog_list 	= compile_prog_list( lex );

	assert( prog_list );

	dump_prog_list( LIST_DUMP_FILE, prog_list );

	FILE*			res_file 	= fopen( argv[2], "w" );
	assert( res_file );

	write_prog_list( res_file, prog_list );

	fclose( res_file );
	destruct_prog_list( prog_list );

	free( lex );
	free( src );

	return 0;
}

